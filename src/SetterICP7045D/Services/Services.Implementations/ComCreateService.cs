﻿using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Services.Implementations
{	
	/// <summary>
	/// Сервис создания коммнанды для модуля ICP DAS 7045D
	/// </summary>
	public class ComCreateService : IComCreateService
	{		
		private readonly ILogger _logger;
		public ComCreateService(ILogger<ComCreateService> logger) 
		{
			_logger = logger;
		}
		public async Task<string> ComCreate(CommandDto input) 
		{
			string ans;
			try
			{
				int new_val = (int)input.NewValue;

                if (input.Param - 108 <= 8)
					ans = $"#{input.Unitid:d2}A{input.Param - 109}{new_val:d2}";
				else
					ans = $"#{input.Unitid:d2}B{input.Param - 109 - 8}{new_val:d2}";
				_logger.LogInformation($"parse result = {ans}");
				return ans;
			}			
			catch ( Exception ex ) 
			{
				_logger.LogInformation($"Eror in command construct, error = {ex.ToString}");
				return "";
			}
		}
			
	}
}
