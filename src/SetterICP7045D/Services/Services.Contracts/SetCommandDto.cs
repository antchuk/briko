﻿using System;
using System.Diagnostics.Eventing.Reader;

namespace Services.Contracts
{
	/// <summary>
	/// модель данных для входящего сообщения из очереди на парсинг
	/// </summary>
	public class SetCommandDto
	{
		public Guid Setup { get; init; }
		public PartType Part { get; init; }
		public int Unitid { get; init; }
		public int Param { get; init; }
		public float NewValue { get; init; }
	}
}
