﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class CommandDto
	{
		public int Unitid { get; init; }
		public int Param { get; init; }
		public float NewValue { get; init; }
	}
}
