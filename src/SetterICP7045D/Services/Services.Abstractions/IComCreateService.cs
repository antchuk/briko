﻿using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
	public interface IComCreateService
	{
		Task<string> ComCreate(CommandDto dto);
	}
}
