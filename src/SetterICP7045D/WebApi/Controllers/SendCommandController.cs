﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;
using Microsoft.Extensions.Logging;
using Services.Implementations;
using Microsoft.Extensions.Options;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер уведомлений
    /// </summary>
    [ApiController]
    [Route("sendcommand")]
    public class SendCommandController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IComCreateService _comCreateService;
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly QueueServiceNames _queuenames;
        public SendCommandController(ILogger<ComCreateService> logger, ISendEndpointProvider sendEndpointProvider,
            IOptions<QueueServiceNames> queuenames)
        {
            _logger = logger;
            _sendEndpointProvider = sendEndpointProvider;
            _queuenames = queuenames.Value;
        }
        /// <summary>
        /// Получить уведомление
        /// </summary>
        /// <param name="_com"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Get([FromBody] SetCommandDto _com)
        {
            if (_com.Setup ==  Guid.Empty) 
            {
                _logger.LogInformation("empty guid");
                return BadRequest();
            }
            var _endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri($"exchange:{_queuenames.QSetICP7045D}"));
            await _endpoint.Send(new SetCommandDto
            {
                NewValue = _com.NewValue,
                Setup = _com.Setup,
                Unitid = _com.Unitid,
                Part = _com.Part,
                Param = _com.Param
            });
            return Ok();
        }        
    }
}
