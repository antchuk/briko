﻿using AutoMapper;
using MassTransit;
using Services.Abstractions;
using Services.Contracts;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System;
using WebApi.Models;
using MassTransit.Configuration;
using MassTransit.Transports;

namespace WebApi.Consumers
{
	/// <summary>
	/// Получатель сообщений
	/// </summary>
	public class QSetICP7045DConsumer : IConsumer<Services.Contracts.SetCommandDto>
	{
		private readonly IComCreateService	_service;
		private readonly Microsoft.Extensions.Logging.ILogger<QSetICP7045DConsumer> _logger;
		private readonly ISendEndpointProvider _sendEndpointProvider;
		private readonly QueueServiceNames _queueNames;

		public QSetICP7045DConsumer(
			IComCreateService service,
			Microsoft.Extensions.Logging.ILogger<QSetICP7045DConsumer> logger,
			ISendEndpointProvider sendEndpointProvider,
			IOptions<QueueServiceNames> queuenames)
		{
			_service = service;
			_logger = logger;
			_sendEndpointProvider = sendEndpointProvider;
			_queueNames = queuenames.Value;
		}
		public async Task Consume(ConsumeContext<Services.Contracts.SetCommandDto> context)
		{
			_logger.LogInformation($"we have got message {context.Message.Part} key {context.Message.Param} = {context.Message.NewValue}");
			if (context == null)
			{
				//послать сообщение об ошибке в очередь ошибок?
				return;
			}
			if (context.Message.Setup == Guid.Empty)
			{
				//послать сообщение об ошибке в очередь ошибок?
				_logger.LogInformation("Empty guid");
                return;
			}
			if (context.Message.Part == 0)
			{
				//послать сообщение об ошибке в очередь ошибок?

				return;
			}
			if (context.Message.NewValue == null)
			{
                //послать сообщение об ошибке в очередь ошибок?
                _logger.LogInformation("Empty new value");
                return;
			}
			_logger.LogInformation($"guid = {context.Message.Setup}\npart = {context.Message.Part}\nunit = {context.Message.Unitid}\n" +
				$"param = {context.Message.Param}\nnewval = {context.Message.NewValue}");
			//сделать новую команду			
			var _icpdata = await _service.ComCreate(new CommandDto { 
				NewValue = context.Message.NewValue,
				Param = context.Message.Param,
				Unitid = context.Message.Unitid				
			});
			//оправить команду дальше? ()
			await SendCommand(_icpdata, context.Message.Setup, context.Message.Unitid, context.Message.Part, context.Message.Param);
		}
		private async Task SendCommand(string com, Guid setup, int unitId, PartType part, int idparam)
		{
			var _endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri($"exchange:{_queueNames.Ogre}"));
			await _endpoint.Send(new DataOut { 
			Command = com,
			Idclient = setup,
			IdUnit = unitId,
			IdPartType = part,
			IdParameter = idparam
			});
		}
	}	
	/// <summary>
	/// не создает дополнителные Exchange/queue в RabbitMQ
	/// </summary>
	public class ParsingICP7045DDefinition : ConsumerDefinition<QSetICP7045DConsumer>
	{
		protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<QSetICP7045DConsumer> consumerConfigurator)
		{
			endpointConfigurator.ConfigureConsumeTopology = false; //не создается Exchange для NotificationDto
		}
	}
}
