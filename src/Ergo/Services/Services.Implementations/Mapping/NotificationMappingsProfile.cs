﻿using AutoMapper;
using Domain.Entities;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations.Mapping
{
    public class NotificationMappingsProfile : Profile
    {
        public NotificationMappingsProfile() 
        {
            CreateMap<Notification, NotificationDto>();

            CreateMap<NotificationDto, Notification>();
                /*.ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore())
                .ForMember(d => d.Lessons, map => map.Ignore());*/
        }
    }
}
