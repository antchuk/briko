﻿using AutoMapper;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Services.Implementations
{
    public class ProxyService : IProxyService
    {
        private readonly INotificationRepository _notificationRepository;
        //private readonly IMapper _mapper;
        //public NotificationService(INotificationRepository notificationRepository, IMapper mapper)
        public ProxyService(INotificationRepository notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }
        public async Task<NotificationDto> GetNotificationById(Guid id)
        {
            var _notification = await _notificationRepository.GetAsync(id);
            if (_notification == null) 
            {
                return null;
            }
            NotificationDto _notificationDto = new()
            { 
                Body=_notification.Body,
                UserID=_notification.UserID,
                SendingDateTime=_notification.SendingDateTime,
                Id = id
            };
            return _notificationDto;
        }
        public async Task<Guid> Create(NotificationDto notificationDto)
        {            
            Notification _notification = new()
            {
                Body = notificationDto.Body,
                UserID = notificationDto.UserID,
                SendingDateTime = notificationDto.SendingDateTime
            };
            var res = await _notificationRepository.CreateAsync(_notification);
            await _notificationRepository.SaveChangesAsync();
            return res.Id;
        }
    }
}
