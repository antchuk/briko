﻿namespace WebApi.Models
{
	/// <summary>
	/// список очередей RabbitMQ
	/// </summary>
	public class QueueNames
	{		
		//public string NotificationCommon { get; set; }
		/// <summary>
		/// выходная очередь
		/// </summary>
		public string QStateData { get; set; }
		/// <summary>
		/// Входная на парсинг очередь
		/// </summary>
		public string ParsingUPC { get; set; }
		/// <summary>
		/// на парсинг ICP7045D
		/// </summary>
		public string QinICP7045D { get; set; }
	}
}
