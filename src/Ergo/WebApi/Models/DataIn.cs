﻿using System;

namespace WebApi.Models
{
	/// <summary>
	/// модель данных для входящего сообщения из очереди на парсинг
	/// </summary>
	public class DataIn
	{
		/// <summary>
		/// клиент
		/// </summary>
		public Guid IDSetup { get; set; }
		/// <summary>
		/// тип устройства
		/// </summary>		
		public PartType IDPartType { get; set; }
		/// <summary>
		/// ид или номер устройства (если их несколько)
		/// </summary>
		public int IDUnit {  get; set; }
		/// <summary>
		/// параметр, если 0 - значит команда общая
		/// </summary>
		public int IdParameter { get; set; }
		/// <summary>
		/// данные для парсинга
		/// </summary>
		public string Data {  get; set; }

		public DataIn(Guid idclient, PartType idPartType, int idUnit, int idParameter, string data)
		{
			IDSetup = idclient;//Guid.Parse(idclient.ToString());
            IDPartType = idPartType;
            IDUnit = idUnit;
            IdParameter = idParameter;
			Data = data;
		}
	}
}
