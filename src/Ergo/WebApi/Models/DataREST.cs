﻿using System;

namespace WebApi.Models
{
	/// <summary>
	/// модель данных для входящего сообщения из очереди на парсинг
	/// </summary>
	public class DataREST
	{
		/// <summary>
		/// клиент
		/// </summary>
		public Guid Idclient { get; set; }
		/// <summary>
		/// тип устройства
		/// </summary>		
		public PartType IdPartType { get; set; }
		/// <summary>
		/// ид или номер устройства (если их несколько)
		/// </summary>
		public int IdUnit {  get; set; }
		/// <summary>
		/// параметр, если 0 - значит команда общая
		/// </summary>
		public int IdParameter { get; set; }
		/// <summary>
		/// данные для парсинга
		/// </summary>
		public string Command {  get; set; }

		/*public DataREST(Guid idclient, PartType idPartType, int idUnit, int idParameter, string data)
		{
            Idclient = idclient;
            IdPartType = idPartType;
			IdUnit = idUnit;
            IdParameter = idParameter;
            Command = data;
		}*/
	}
}
