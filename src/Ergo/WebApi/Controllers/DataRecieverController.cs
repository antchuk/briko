﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер уведомлений
    /// </summary>
    [ApiController]
    [Route("datareciever")]
    public class DataRecieverController : ControllerBase
    {
        private readonly IProxyService _service;
        private readonly IMapper _mapper;
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly QueueNames _queueNames;
		private readonly Microsoft.Extensions.Logging.ILogger<DataRecieverController> _logger;
		public DataRecieverController(IProxyService service, IMapper mapper, ISendEndpointProvider sendEndpointProvider, IOptions<QueueNames> options, 
            Microsoft.Extensions.Logging.ILogger<DataRecieverController> logger)
        {
            _service = service;
            _mapper = mapper;
            _sendEndpointProvider = sendEndpointProvider;
            _queueNames = options.Value;
            _logger = logger;
        }
		/// <summary>
		/// СПришли данные
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		[HttpPost]
        public async Task<IActionResult> Add([FromBody] DataREST data)
        {
            if (data == null || data.Idclient==Guid.Empty) 
            {
                _logger.LogInformation($"get null or empty GUID");
                return  BadRequest(); 
            }
            _logger.LogInformation($"get meassage = {data.Command}");
            await SendToQueue(data);
            return Ok();
        }
        /// <summary>
        /// Отправка сообщения в EndPoint(queue or Exchange)
        /// </summary>
        /// <returns></returns>
        private async Task SendToQueue(DataREST data) //CancellationToken stoppingtoken
        {
            //ЗАГЛУШКА потом выбор очереди - по словарю (словарь грузить из БД)
            string qname;            
            switch (data.IdPartType)
            {
				case PartType.UPC:
					qname = _queueNames.ParsingUPC;
					break;
				case PartType.Switcher:
					qname = _queueNames.QinICP7045D;
					break;
                default:
                    qname = "";
                    break;
			}
            if (qname != "")
            {
                var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri($"exchange:{qname}"));
                await endpoint.Send(new DataIn(data.Idclient, data.IdPartType, data.IdUnit, data.IdParameter, data.Command));
            }
            else
            {
                _logger.LogInformation($"wrong queue, IDPartType = {data.IdPartType}, IDSetup {data.Idclient}, data = {data.Command}");
            }

        }
    }
}
