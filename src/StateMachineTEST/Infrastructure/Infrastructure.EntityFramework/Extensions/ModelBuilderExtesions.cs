﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.EntityFramework.Extensions
{
	public static class ModelBuilderExtesions
	{
		public static void Seed(this ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Settings>()
				.HasData(
				new Settings
				{
					Id = Guid.NewGuid(),
					Setup = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
					PartType = Services.Contracts.PartType.Switcher,
					Parameter = 109,
					PValue = 0,
					UnitId = 0,
					SettingsType = Services.Contracts.SettingsType.Mins
				});
			modelBuilder.Entity<Settings>()
				.HasData(
				new Settings
				{
					Id = Guid.NewGuid(),
					Setup = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
					PartType = Services.Contracts.PartType.Switcher,
					Parameter = 109,
					PValue = 1,
					UnitId = 0,
					SettingsType = Services.Contracts.SettingsType.Maxs
				});
			modelBuilder.Entity<Settings>()
				.HasData(
				new Settings
				{
					Id = Guid.NewGuid(),
					Setup = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
					PartType = Services.Contracts.PartType.Switcher,
					Parameter = 109,
					PValue = 1,
					UnitId = 0,
					SettingsType = Services.Contracts.SettingsType.StepMinus
				});
			modelBuilder.Entity<Settings>()
				.HasData(
				new Settings
				{
					Id = Guid.NewGuid(),
					Setup = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
					PartType = Services.Contracts.PartType.Switcher,
					Parameter = 109,
					PValue = 1,
					UnitId = 0,
					SettingsType = Services.Contracts.SettingsType.StepPlus
				});
		}
	}
}
