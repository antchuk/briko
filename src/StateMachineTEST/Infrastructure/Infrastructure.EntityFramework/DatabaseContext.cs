﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Infrastructure.EntityFramework.Extensions;
using Npgsql;

namespace Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {      
        
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureDeletedAsync().Wait();
            //Database.EnsureCreated();
        }
        /// <summary>
        /// все настройки максимальных значений
        /// </summary>
        public DbSet<Settings> MaxMinTarStepSettings { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseIdentityColumns();

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Settings>().HasKey(x => x.Id);
            modelBuilder.Entity<Settings>().Property(x => x.Id).ValueGeneratedOnAdd();

			modelBuilder.Seed();
        }
    }
}