﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
	/// <summary>
	/// репозиторий настроек машины
	/// </summary>
	public class SettingsRepository: Repository<Settings, Guid>, ISettingsRepository
	{
		public SettingsRepository(DatabaseContext context) : base(context) 
		{
		}
		public async Task<List<Settings>> GetSetAsync(Guid id, SettingsType type)
		{
			var res = GetAll().ToList().AsQueryable();
			res = from set in res
				  where set.SettingsType == type
				  select set;
			return res.ToList();
		}

	}
}
