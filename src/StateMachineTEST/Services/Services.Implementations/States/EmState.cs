﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Implementations;

namespace Services.Implementations.States
{
    public class EmState : State
    {
        public EmState()
        {
            if (_context != null)
            {
                _context.LogSome("Em state created");
            }
		}
        public override void SS_handle()
        {
			_context.LogSome("SS state");			
		}
        public override void Er_handle()
        {
			//выключить все узлы измеряющего типа
			_context.LogSome("Error state");			
		}
        public override void Em_handle()
        {
            //выключить все узлы измеряющего типа
            _context.LogSome("Em do nothing");
            //перевести всё в состояние максимально-безопасного функционирования			
        }
        public override void ON_handle()
        {
			_context.LogSome("not allowed, solve the problem");
		}
        public override void OFF_handle()
        {
			//выключаем все узлы установки
			foreach (var part in _context.Parts.Values)
			{
				part.TurnOff();
			}
		}
    }
}
