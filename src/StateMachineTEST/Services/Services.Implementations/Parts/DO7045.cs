﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Implementations.Parts
{
	public class DO7045 : SetupPart
	{
		public DO7045(ILogger logger, string name, StateMachine setup, PartType type, int unitid, bool isCritical) : base(logger, name, setup, type, unitid, isCritical)
		{
			_logger.LogInformation($"{DateTime.UtcNow} DO7045 created");
		}
		public override void TurnOn()
		{
			if (StateOnOff)
			{
				_logger.LogInformation($"{DateTime.UtcNow} DO7045 already ON");
			}
			else
			{
				StateOnOff = true;
				_logger.LogInformation($"{DateTime.UtcNow} DO7045 turn ON");
				_Setup.UpdateTarget(_Setup.Id, PartType.Switcher, 4, 109, 1);
            }
		}
		public override void TurnOff()
		{
			if (StateOnOff)
			{
				StateOnOff = false;
				_logger.LogInformation($"{DateTime.UtcNow} DO7045 turned OFF");
			}
			else
			{
				_logger.LogInformation($"{DateTime.UtcNow} DO7045 already OFF");
			}
		}
		public override bool CanDoStep(int param)
		{
			//switch case в зависимости от логики и распиновки
			return true;
		}
	}
}
