﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Implementations.Parts
{
	public abstract class SetupPart
	{
		public readonly ILogger _logger;
		public StateMachine _Setup;
		public ConcurrentDictionary<int, float> Data; //параметры системы
		public readonly bool IsCritical;
		public SetupPart(ILogger logger, string name, StateMachine setup, PartType type, int unitid, bool isCritical)
		{
			_logger = logger;
			Name = name;
			_Setup = setup;
			StateOnOff = false;
			UnitId = unitid;
			Type = type;
			IsCritical = isCritical;
			Data = new ConcurrentDictionary<int, float>();
		}
		/// <summary>
		/// Имя
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// включен или выключен узел
		/// </summary>
		public bool StateOnOff { get; set; }
		/// <summary>
		/// Тип узла
		/// </summary>
		public PartType Type { get; set; }
		public int UnitId { get; set; }
		/// <summary>
		/// проверить всё ли хорошо по параметрам
		/// </summary>
		/// <returns></returns>
		public bool HealthCheck()
		{
			if (_Setup == null)
			{
				return false;
			}
			if (_Setup.Mins == null || 
				_Setup.Maxs == null ||
				_Setup.Mins[Type] == null ||
				_Setup.Maxs[Type] == null ||
				_Setup.Mins[Type][UnitId] == null ||
				_Setup.Maxs[Type][UnitId] == null )
			{
				return false;
			}			
			foreach (var param in Data)
			{
				if (_Setup.Mins.ContainsKey(Type) &&
					_Setup.Maxs.ContainsKey(Type) &&
					_Setup.Mins[Type].ContainsKey(UnitId) &&
					_Setup.Maxs[Type].ContainsKey(UnitId) &&
					_Setup.Mins[Type][UnitId].ContainsKey(param.Key) &&
					_Setup.Maxs[Type][UnitId].ContainsKey(param.Key))
				{
					if (param.Value < _Setup.Mins[Type][UnitId][param.Key] ||
						param.Value > _Setup.Maxs[Type][UnitId][param.Key])
					{
						return false;
					}
				}
			}
			return true;
		}
		public float GetNewTargetValue(int par)
		{			
			float ans = _Setup.Parts[Type].Data[par];
			//если меньше мин - сразу меняем
			if (ans < _Setup.Mins[Type][UnitId][par])
			{
				ans -= _Setup.StepPlus[Type][UnitId][par];
				//проверка на выход за диапазон
				if (ans > _Setup.Maxs[Type][UnitId][par]) ans = _Setup.Maxs[Type][UnitId][par];
				//Log(true, "Debug", true, $"param {_ID} MIN limit exceeded");
			}
			else
			{
				//если больше макс - сразу меняем
				if (ans > _Setup.Maxs[Type][UnitId][par])
				{
					ans -= _Setup.StepMinus[Type][UnitId][par];
					//проверка на выход за диапазон
					if (ans < _Setup.Mins[Type][UnitId][par]) ans = _Setup.Mins[Type][UnitId][par];
					//Log(true, "Debug", true, $"param {_ID} MAX limit exceeded");
				}
				else
				{
					//все хорошо, нужно ли делать шаг?
					if (_Setup.Targets[Type][UnitId].ContainsKey(par))
					{
						//расчитываем делать в какую сторону шаг                        
						ans = CalcStep(par);
					}
				}
			}
			return ans;
		}
		public virtual float CalcStep(int par)
		{
			float ans = _Setup.Parts[Type].Data[par];
			if (ans < _Setup.Targets[Type][UnitId][par] &&
						ans + _Setup.StepPlus[Type][UnitId][par] <= _Setup.Targets[Type][UnitId][par] &&
						ans + _Setup.StepPlus[Type][UnitId][par] <= _Setup.Maxs[Type][UnitId][par])
			{
				ans += _Setup.StepPlus[Type][UnitId][par];
			}
			if (ans > _Setup.Targets[Type][UnitId][par] &&
				ans - _Setup.StepMinus[Type][UnitId][par] >= _Setup.Targets[Type][UnitId][par] &&
				ans - _Setup.StepMinus[Type][UnitId][par] >= _Setup.Mins[Type][UnitId][par])
			{
				ans -= _Setup.StepMinus[Type][UnitId][par];
			}
			return ans;
		}
		public abstract bool CanDoStep(int param);
		/// <summary>
		/// включить
		/// </summary>
		public abstract void TurnOn();
		/// <summary>
		/// выключить
		/// </summary>
		public abstract void TurnOff();

		

	}
}
