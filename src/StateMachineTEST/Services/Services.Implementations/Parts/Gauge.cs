﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Implementations.Parts
{
	public class Gauge : SetupPart
	{
		public Gauge(ILogger logger, string name, StateMachine setup, PartType type, int unitid, bool isCritical) : base(logger, name, setup, type, unitid, isCritical)
		{
			_logger.LogInformation($"{DateTime.UtcNow} Gauge created");
			this.Type = PartType.Gauge;
		}
		//public float Pressure { get; set; }
		public override void TurnOn()
		{
			if (StateOnOff)
			{				
				_logger.LogInformation($"{DateTime.UtcNow} Gauge already ON");
			}
			else
			{
				StateOnOff = true;
				_logger.LogInformation($"{DateTime.UtcNow} Gauge turn ON");
			}
			/*if (_Setup.Parts.ContainsKey(PartType.Switcher))
			{
				_logger.LogInformation("we send command to switcher");
				StateOnOff = state;
			}
			else
			{
				_logger.LogInformation("no switcher, so do nothing");
			}*/
		}
		public override void TurnOff()
		{
			if (StateOnOff)
			{
				StateOnOff = false;
				_logger.LogInformation($"{DateTime.UtcNow} Gauge turned OFF");
			}
			else
			{
				_logger.LogInformation($"{DateTime.UtcNow} Gauge already OFF");
			}
		}
		public override bool CanDoStep(int param)
		{

			return false;
		}
	}
}
