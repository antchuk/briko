﻿using Domain.Entities;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
	public interface ISettingsRepository : IRepository<Settings, Guid>
	{
		Task<List<Settings>> GetSetAsync(Guid setup, SettingsType type);
	}
}
