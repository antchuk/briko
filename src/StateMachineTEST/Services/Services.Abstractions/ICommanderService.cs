﻿//using Microsoft.EntityFrameworkCore.Migrations.Operations;

using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstractions
{
	public interface ICommanderService
	{
		/// <summary>
		/// ИД установки, тип узла, номер узла, ИД параметра, значение
		/// </summary>
		/// <param name="id"></param>
		/// <param name="part"></param>
		/// <param name="unitId"></param>
		/// <param name="parameter"></param>
		/// <param name="newValue"></param>
		/// <returns></returns>
		public Task SendCommand(Guid id, PartType part, int unitId, int parameter, float newValue);		
	}
}
