﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер состояния о установке
    /// </summary>
    [ApiController]
    [Route("StateData")]
    public class StateDataController : ControllerBase
    {
        //private readonly IStateMachineService _service;
        private readonly IStateMachine _setup;
        public StateDataController( IStateMachine setup)//IStateMachineService service,
		{
            //_service = service;
            _setup = setup;
            //_mapper = mapper;
        }
        /// <summary>
        /// запускает машину
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("start/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            if (id ==  Guid.Empty) 
            {
                return BadRequest();
            }
            if (_setup.IsRunning(id))
            {
                return Ok();
            }
            else
            {
                _setup.SetGuid(id);
				await _setup.AllOnSetup();
			}            
            return Ok();
        }
        /// <summary>
        /// остановить опрос машины
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("stop/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (_setup.IsRunning(id))
            {
                _setup.StopAskingTimer();
                return Ok();
            }
            else
                return BadRequest();
        }
		/// <summary>
		/// изменить целевое значение параметра
		/// </summary>
		/// <param name="setup"></param>
		/// <param name="partType"></param>
		/// <param name="unitId"></param>
		/// <param name="key"></param>
		/// <param name="newValue"></param>
		/// <returns></returns>
		[HttpPost("updatedata/")]
		public async Task<IActionResult> PostTarget([FromBody] Guid setup, PartType partType, int unitId, int key, float newValue)
		{
			if (setup == Guid.Empty || unitId <0 || key <0)
			{
				return BadRequest();
			}
            await _setup.UpdateTarget(setup, partType, unitId, key, newValue);
			return Ok();
		}
	}
}
