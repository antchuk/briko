﻿using AutoMapper;
using MassTransit;
using Services.Abstractions;
using Services.Contracts;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System;
using WebApi.Models;
using MassTransit.Configuration;

namespace WebApi.Consumers
{
	/// <summary>
	/// Получатель сообщений
	/// </summary>
	public class ParsingICP7045DConsumer : IConsumer<DataIn>
	{
		private readonly IParserService	_service;
		private readonly IMapper _mapper;
		private readonly Microsoft.Extensions.Logging.ILogger<ParsingICP7045DConsumer> _logger;
		private readonly ISendEndpointProvider _sendEndpointProvider;
		private readonly QueueNames _queueNames;

		public ParsingICP7045DConsumer(
			IParserService service,
			IMapper mapper,
			Microsoft.Extensions.Logging.ILogger<ParsingICP7045DConsumer> logger,
			ISendEndpointProvider sendEndpointProvider,
			IOptions<QueueNames> queuenames)
		{
			_service = service;
			_mapper = mapper;
			_logger = logger;
			_sendEndpointProvider = sendEndpointProvider;
			_queueNames = queuenames.Value;
		}
		public async Task Consume(ConsumeContext<DataIn> context)
		{
			_logger.LogInformation($"we have got message {context.Message.Data}");
			if (context == null)
			{
				//послать сообщение об ошибке в очередь ошибок?
				return;
			}
			if (context.Message.IDSetup == Guid.Empty)
			{
                _logger.LogInformation($"empty Guid");
                //послать сообщение об ошибке в очередь ошибок?
                return;
			}
			if (context.Message.IDPartType == 0)
			{
                _logger.LogInformation($"empty PartType");
                //послать сообщение об ошибке в очередь ошибок?
                return;
			}
			if (context.Message.Data == null)
			{
				//послать сообщение об ошибке в очередь ошибок?
				return;
			}
			//получить
			//здеcь идем в сервис за парсингом
			var _icpdata = await _service.Parse(context.Message.Data);
			if (_icpdata.succed)
			{
				_logger.LogInformation($"we parse DIO channel 0 {_icpdata.Data[109]}");
				//отправляем в очередь qStateData
				var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri($"exchange:{_queueNames.QStateData}"));
				await endpoint.Send<ICPDto>(new ICPDto(_icpdata.succed, context.Message.IDSetup, context.Message.IDPartType, context.Message.IDUnit, _icpdata.Data));
			}
			else
			{
				_logger.LogInformation($"parsing problem in client = {context.Message.IDSetup} par = {context.Message.IDPartType}");
			}
		}
		
	}
	/// <summary>
	/// не создает дополнителные Exchange/queue в RabbitMQ
	/// </summary>
	public class ParsingICP7045DDefinition : ConsumerDefinition<ParsingICP7045DConsumer>
	{
		protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<ParsingICP7045DConsumer> consumerConfigurator)
		{
			endpointConfigurator.ConfigureConsumeTopology = false; //не создается Exchange для NotificationDto
		}
	}
}
