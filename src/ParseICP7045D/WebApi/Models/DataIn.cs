﻿using System;

namespace WebApi.Models
{
	/// <summary>
	/// модель данных для входящего сообщения из очереди на парсинг
	/// </summary>
	public record DataIn
	{
		/// <summary>
		/// клиент
		/// </summary>
		public Guid IDSetup { get; init; }
		/// <summary>
		/// номер парсера
		/// </summary>
		public int IDPartType { get; init; }
		/// <summary>
		/// номер узла (если их несколько)
		/// </summary>
		public int IDUnit { get; init; }
		/// <summary>
		/// данные для парсинга
		/// </summary>
		public string Data { get; init; }
	}
}
