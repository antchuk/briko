﻿using System;

namespace WebApi.Models
{
    /// <summary>
    /// Уведомление
    /// </summary>
    public class NotificationModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid Id { get; init; }
        /// <summary>
        /// Body of message, preferable html
        /// </summary>
        public string Body { get; init; }
        /// <summary>
        /// sending(recieving by RabbitMQ?) DateTime 
        /// </summary>
        public DateTime SendingDateTime { get; init; }
        /// <summary>
        /// ID of User
        /// </summary>
        public Guid UserID { get; init; }
        /// <summary>
        /// address
        /// </summary>
        public string Address {  get; init; }
    }

}
