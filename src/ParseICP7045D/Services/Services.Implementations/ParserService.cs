﻿using Microsoft.IdentityModel.Tokens;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Services.Implementations
{	
	/// <summary>
	/// Сервис парсинга ответа модуля UPC
	/// </summary>
	public class ParserService : IParserService
	{
		
		public ParserService() 
		{

		}
		public async Task<ICPparams> Parse(string input) 
		{
			var ans = await ParseAsync(input);
			return ans;
		}
		private Task<ICPparams> ParseAsync(string input) 
		{
			/*if (input == null) 
			{
				//сообщение?
				return Task.FromResult(-999f);
			}
			if (input.Length < 37)
			{
				//сообщение?
				return Task.FromResult(-999f);
			}*/
			ICPparams Upc = Parsedata(input);
			if (!Upc.succed)
			{
				return Task.FromResult(Upc);
			}
			return Task.FromResult(Upc);
		}
		/// <summary>
		/// парсинг строки на выход параметры 109-124
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		private ICPparams Parsedata(string input)
		{
			try
			{
				Dictionary<int, float> _temp = new();
                string _answer = input.Substring(1, 4);
                int i_ans = int.Parse(_answer, System.Globalization.NumberStyles.HexNumber);
                _answer = Convert.ToString(i_ans, 2);
                _answer = _answer.PadLeft(16, '0');
                char[] rev_ans = _answer.ToCharArray(); //ToArray();
                Array.Reverse(rev_ans);

                for (int i=0;i<=15; i++)
				{
					_temp.Add(109 + i, float.Parse(rev_ans[i].ToString(), NumberStyles.Any, CultureInfo.InvariantCulture));
				}
				ICPparams _icpData = new() 
				{
					succed = true,
					Data = _temp
				};
				return _icpData;
			}
			catch (FormatException exx)
			{
				//лог о ошибке
				
				return null;
			}
			catch (Exception ex)
			{
				//лог о ошибке
				
				return null;
			}			
		}
	}
}
