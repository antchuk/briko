﻿using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
	public interface IParserService
	{
		Task<ICPparams> Parse (string input);
		//public Task Send(UPCdto data);
	}
}
