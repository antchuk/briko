﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;
using Infrastructure.EntityFramework;
using System.Threading.Tasks;
using System;

namespace Services.Repositories.Implementations
{
    public class NotificationRepository : INotificationRepository
    {
        public readonly DbContext Context;
        private readonly DbSet<Notification> _notificationSet;
        public NotificationRepository(DatabaseContext context)
        {
            Context = context;
            _notificationSet = Context.Set<Notification>();
        }
        public async Task<Notification> GetAsync(Guid id)
        {
            return await _notificationSet.FindAsync(id);
        }
        public async Task<Notification> CreateAsync(Notification _notification)
        {
            return (await _notificationSet.AddAsync(_notification)).Entity;
        }
        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
        public async Task<bool> DeleteAsync(Guid id)
        {
            var _res = await _notificationSet.FindAsync(id);
            if (_res != null) 
            {
                _notificationSet.Remove(_res);
                return true;
            }
            return false;
        }
        public async Task<Notification> UpdateAsync(Notification _notification)
        {
            return _notificationSet.Update(_notification).Entity;
        }
    }
}