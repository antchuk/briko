﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер уведомлений
    /// </summary>
    [ApiController]
    [Route("proxyout")]
    public class DataRecieverController : ControllerBase
    {
        private readonly IProxyService _service;
        private readonly IMapper _mapper;
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly QueueNames _queueNames;
		private readonly Microsoft.Extensions.Logging.ILogger<DataRecieverController> _logger;
		public DataRecieverController(IProxyService service, IMapper mapper, ISendEndpointProvider sendEndpointProvider, IOptions<QueueNames> options, 
            Microsoft.Extensions.Logging.ILogger<DataRecieverController> logger)
        {
            _service = service;
            _mapper = mapper;
            _sendEndpointProvider = sendEndpointProvider;
            _queueNames = options.Value;
            _logger = logger;
        }
		/// <summary>
		/// СПришли данные
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		[HttpPost]
        public async Task<IActionResult> Add([FromBody] DataREST data)
        {
            if (data == null || data.Idclient==Guid.Empty) 
            {
                _logger.LogInformation($"get null or empty GUID");
                return  BadRequest(); 
            }
            _logger.LogInformation($"get meassage = {data.Data}");
            //await SendToQueue(data);
            return Ok();
        }
        /// <summary>
        /// Отправка сообщения в EndPoint(queue or Exchange)
        /// </summary>
        /// <returns></returns>
        private async Task SendToQueue(DataREST data) //CancellationToken stoppingtoken
        {
            

        }
    }
}
