﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Services.Contracts;
using System.Threading.Tasks;
using System;
using Services.Abstractions;

namespace WebApi.Consumer
{
    public class QStringComConsumer: IConsumer<DataOut>
    {
        private readonly IProxyService _proxyservice;
        private readonly Microsoft.Extensions.Logging.ILogger<QStringComConsumer> _logger;
        //private readonly ISendEndpointProvider _sendEndpointProvider;
        //private readonly QueueServiceNames _queueNames;
        public QStringComConsumer(ILogger<QStringComConsumer> logger, IProxyService proxyService)
        {
            _logger = logger;
            _proxyservice = proxyService;
        }
        public async Task Consume(ConsumeContext<Services.Contracts.DataOut> context)
        {
            _logger.LogInformation($"we have got message {context.Message.IdPartType} command = {context.Message.Command}");
            if (context == null)
            {
                //послать сообщение об ошибке в очередь ошибок?
                return;
            }
            if (context.Message.Idclient == Guid.Empty)
            {
                //послать сообщение об ошибке в очередь ошибок?
                _logger.LogInformation("Empty guid");
                return;
            }
            if (context.Message.IdPartType == 0)
            {
                //послать сообщение об ошибке в очередь ошибок?

                return;
            }
            if (context.Message.Command == null)
            {
                //послать сообщение об ошибке в очередь ошибок?
                _logger.LogInformation("Empty new value");
                return;
            }
            _logger.LogInformation($"guid = {context.Message.Idclient}\npart = {context.Message.IdPartType}\nunit = {context.Message.IdUnit}\n" +
                $"param = {context.Message.IdParameter}\ncommand = {context.Message.Command}");
            _proxyservice.SendCommand(context.Message.Idclient, context.Message.IdPartType, context.Message.IdUnit, context.Message.IdParameter, context.Message.Command);
        }
    }
    /// <summary>
	/// не создает дополнителные Exchange/queue в RabbitMQ
	/// </summary>
	public class QStringComDefinition : ConsumerDefinition<QStringComConsumer>
    {
        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<QStringComConsumer> consumerConfigurator)
        {
            endpointConfigurator.ConfigureConsumeTopology = false; //не создается Exchange для NotificationDto
        }
    }
}
