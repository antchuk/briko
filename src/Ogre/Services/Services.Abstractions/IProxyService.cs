﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Сервис работы с уведомлениями
    /// </summary>
    public interface IProxyService
    {
        Task<bool> SendCommand(Guid idclient, PartType partType, int unitId, int parameter, string command);
        //Task<Guid> Create( );
    }
}
