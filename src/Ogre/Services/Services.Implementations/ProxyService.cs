﻿using AutoMapper;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using System.Net.Http.Json;

namespace Services.Implementations
{
    public class ProxyService : IProxyService
    {
        //private readonly INotificationRepository _notificationRepository;
        IHttpClientFactory _httpClientFactory;
        private readonly Microsoft.Extensions.Logging.ILogger<ProxyService> _logger;
        public ProxyService(IHttpClientFactory httpClientFactory, ILogger<ProxyService> logger)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }
        public async Task<bool> SendCommand(Guid idclient, PartType partType, int unitId, int parameter, string command)
        {
            using HttpClient _client = _httpClientFactory.CreateClient();
            try
            {
                var message = new DataOut
                {
                    Idclient = idclient,
                    IdPartType = partType,
                    IdUnit = unitId,
                    IdParameter = parameter,
                    Command = command
                };
                using var sendResult = await _client.PostAsJsonAsync<DataOut>(GetUrlByGuid(idclient), message);
                if (!sendResult.IsSuccessStatusCode)
                {
                    var errorResult = await sendResult.Content?.ReadFromJsonAsync<DataOut>();
                    _logger.LogInformation($"Не удалось выполнить запрос {(int)sendResult.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation("Error sending post to COMTripper: {Error}", e);
            }

            return true;
        }
        private string GetUrlByGuid(Guid id)
        {
            return "http://host.docker.internal:7000/recievecommand";
        }
    }
}
