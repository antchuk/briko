﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations.Exceptions;

internal class ItemAlreadyExistException : HttpException
{
    private const string MESSAGE = "Элемент {0} с Id {1} уже существует в базе";

    public ItemAlreadyExistException(Guid id, string itemName)
        : base(
              httpStatusCode: System.Net.HttpStatusCode.Conflict, //Net7 отключили Microsoft.AspNetCore.Http ???
              message: string.Format(MESSAGE, itemName, id))
    {
    }
}