﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class DataOut
    {
        public Guid Idclient { get; init; }
        /// <summary>
        /// тип устройства
        /// </summary>		
        public PartType IdPartType { get; init; }
        /// <summary>
        /// ид или номер устройства (если их несколько)
        /// </summary>
        public int IdUnit { get; init; }
        /// <summary>
        /// параметр, если 0 - значит команда общая
        /// </summary>
        public int IdParameter { get; init; }
        /// <summary>
        /// данные для парсинга
        /// </summary>
        public string Command { get; init; }
    }
}
