﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class UPCdto
	{
		public float I_p_voltage;
		public float I_p_f_voltage;
		public float O_p_voltage;
		public int O_p_current;
		public float I_p_frequency;
		public int Bat_level;
		public float Temperature;
		public int ups_fault;
		public bool succed;
	}
}
