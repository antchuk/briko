﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.StateMachine.Parts
{
	public class UPC : SetupPart
	{
		public UPC(ILogger logger, string port, string name, StateMachine _setup) : base(logger, port, name, _setup)
		{
			_logger.LogInformation("UPC created");
			this.Type = PartType.UPC;
		}
		public override bool Switch(bool state)
		{
			StateOnOff = !StateOnOff;
			_logger.LogInformation($"UPC switch on/off = {StateOnOff}");
			return true;
		}
	}
}
