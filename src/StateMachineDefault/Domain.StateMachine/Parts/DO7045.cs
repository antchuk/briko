﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.StateMachine.Parts
{
	public class DO7045 : SetupPart
	{
		public DO7045(ILogger logger, string port, string name, StateMachine _setup) : base(logger, port, name, _setup)
		{
			_logger.LogInformation("DO7045 created");
			//this.Type = PartType.DO7045;
		}
		public override bool Switch(bool state)
		{
			StateOnOff = !StateOnOff;
			_logger.LogInformation($"DO7045 switch on/off = {StateOnOff}");
			return true;
		}
	}
}
