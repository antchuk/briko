﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.StateMachine.Parts
{
	public class Gauge : SetupPart
	{
		public Gauge(ILogger logger, string port, string name, StateMachine _setup) : base(logger, port, name, _setup)
		{
			_logger.LogInformation("Gauge created");
			this.Type = PartType.Gauge;
		}
		//public float Pressure { get; set; }
		public override bool Switch(bool state)
		{
			//проверяем условия
			//switcher - существует
			//какое его состояние, если вкл - то выключаем
			//если выключен - команда на выключение датчика.

			//если условия норма- просто меняем таргеты в стайт машине


			if (_Setup.Parts.ContainsKey(PartType.Switcher))
			{
				_logger.LogInformation("we send command to switcher");
				StateOnOff = state;
			}
			else
			{
				_logger.LogInformation("no switcher, so do nothing");
			}
			return true;
		}
	}
}
