﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.StateMachine.Parts
{
	public abstract class SetupPart
	{
		public readonly ILogger _logger;
		public readonly string _COMPort;
		public StateMachine _Setup;
		public SetupPart(ILogger logger, string port, string name, StateMachine setup)
		{
			_logger = logger;
			_COMPort = port;
			Name = name;
			_Setup = setup;
			StateOnOff = false;
		}
		/// <summary>
		/// Имя
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// включен или выключен узел
		/// </summary>
		public bool StateOnOff { get; set; }
		/// <summary>
		/// Тип узла
		/// </summary>
		public PartType Type { get; init; }
		/// <summary>
		/// настройки вставить сюда
		/// </summary>
		public string COMPortName { get; set; }
		public abstract bool Switch(bool state);
		//public abstract void TurnOn();
		//public abstract void TurnOff();
		//public abstract void PushTarget(int key, float value);
		public Dictionary<int, float> Data; //параметры системы

	}
}
