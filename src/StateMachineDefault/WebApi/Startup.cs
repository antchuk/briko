using AutoMapper;
using Castle.Core.Configuration;
using MailKit;
using MassTransit;
using MassTransit.RabbitMqTransport;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations;
using System.IO;
using System.Reflection;
using System;
//using WebApi.Consumers;
using WebApi.Settings;
using static MassTransit.Logging.OperationName;
using Infrastructure.EntityFramework;
using WebApi.Models;
using Services.Contracts;

namespace WebApi
{
    public class Startup
    {
        public Startup(Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public Microsoft.Extensions.Configuration.IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddServices(Configuration);
            services.AddControllers();
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(options => {
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));                
            });
            services.Configure<QueueNames>(Configuration.GetSection(nameof(QueueNames)));
			services.Configure<QueueServiceNames>(Configuration.GetSection(nameof(QueueServiceNames)));
			services.Configure<StateMachineSettings>(Configuration.GetSection(nameof(StateMachineSettings)));
			services.AddHttpClient();
//			services.AddSingleton<IStateMachine, Services.Implementations.StateMachine>();

			services.AddMassTransit(x => 
            {
                //x.AddConsumer<QStateDataConsumer, QStateDataDefinition>();
                x.UsingRabbitMq((context, cfg) =>
                {
                    Configure(cfg, Configuration);
                    x.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter(false)); // уточняем формат названия очереди и точки обмена                  
                    cfg.ConfigureEndpoints(context);  //consumer registration 
                });                
            }
            );
            services.AddCors();            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DatabaseContext context)
        {
            context.Database.EnsureCreated();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseHealthChecks("/health");

            app.UseRouting();

            app.UseAuthorization();

            if (!env.IsProduction())
            {
                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
                // specifying the Swagger JSON endpoint.
               
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }        

        /// <summary>
        /// Кoнфигурирование
        /// </summary>
        /// <param name="configurator"></param>
        /// <param name="conf"></param>
        private static void Configure(IRabbitMqBusFactoryConfigurator configurator , Microsoft.Extensions.Configuration.IConfiguration conf)
        {
            configurator.Host(conf["RabbitMq:Host"],  
                conf["RabbitMq:VirtualHost"],
                h =>
                {
                    h.Username(conf["RabbitMq:Username"]);
                    h.Password(conf["RabbitMq:Password"]);                    
                });
        }
    }
    
}