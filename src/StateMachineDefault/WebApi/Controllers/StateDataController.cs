﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер состояния о установке
    /// </summary>
    [ApiController]
    [Route("StateData")]
    public class StateDataController : ControllerBase
    {
        //private readonly IStateMachineService _service;
        private readonly IStateMachine _setup;
        private readonly ISchemasService _schemasService;
        public StateDataController( IStateMachine setup, ISchemasService schemasService)//IStateMachineService service,
		{
            //_service = service;
            _setup = setup;
            _schemasService = schemasService;
            //_mapper = mapper;
        }
        /// <summary>
        /// получить данные о установке с ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getfulldata/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            if (id ==  Guid.Empty) 
            {
                return BadRequest();
            }
            if (!_setup.IsRunning(id))
            {
                _setup.SetGuid();
			}  
            //отправить запрос в State
            var _data = await _setup.GetStateInfo(id);
            return Ok(_data);
        }        
		/// <summary>
		/// изменить целевое значение параметра
		/// </summary>
		/// <param name="setup"></param>
		/// <param name="partType"></param>
		/// <param name="unitId"></param>
		/// <param name="key"></param>
		/// <param name="newValue"></param>
		/// <returns></returns>
		[HttpPost("updatedata/")]
		public async Task<IActionResult> PostTarget(Guid setup, PartType partType, int unitId, int key, float newValue)
		{
			if (setup == Guid.Empty || unitId <0 || key <0)
			{
				return BadRequest();
			}
            await _setup.UpdateTarget(setup, partType, unitId, key, newValue);
			return Ok();
		}
	}
}
