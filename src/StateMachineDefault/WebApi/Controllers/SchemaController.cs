﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер состояния о установке
    /// </summary>
    //[ApiController]
    [Route("Schema")]
    public class SchemaController : ControllerBase
    {
        private readonly ISchemasService _schemasService;
        public SchemaController( ISchemasService schemasService)
		{
            _schemasService = schemasService;
        }
        /// <summary>
        /// получить схему установки
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getschema/{id}")]
        public async Task<IActionResult> GetSchema(Guid id)
        {
            var res = await _schemasService.GetSchema(id);
            if (res == null) { return NotFound(); }
            return Ok(res);
        }
        /// <summary>
        /// создать схему устанвоки(!!!нет проверки на уникальность!!!)
        /// </summary>
        /// <param name="setup"></param>
        /// <param name="partType"></param>
        /// <param name="unitId"></param>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <param name="decription"></param>
        /// <returns></returns>
        [HttpPost("postschema/")]
        public async Task<IActionResult> PostSchema(Guid setup, PartType partType, int unitId, string name, string decription, int paramId, int paramValue)
        {
            if (setup==Guid.Empty)
            {
                return BadRequest();
            }            
            await _schemasService.CreateSchema(setup, partType, unitId, name, decription, paramId, paramValue);

            return Ok();
        }
	}
}
