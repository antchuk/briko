﻿namespace WebApi.Models
{
	/// <summary>
	/// список очередей RabbitMQ
	/// </summary>
	public class QueueNames
	{		
		/// <summary>
		/// очередь к данным
		/// </summary>
		public string QStateData { get; set; }
	}
}
