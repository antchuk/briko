﻿using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Settings : IEntity<Guid>
	{
		public Guid Id {  get; init; }
		public Guid Setup {  get; set; }
		public PartType PartType { get; set; }
		public int UnitId { get; set; }
		public int Parameter {  get; set; }
		public float PValue { get; set; }
		public SettingsType SettingsType { get; set; }
	}
}
