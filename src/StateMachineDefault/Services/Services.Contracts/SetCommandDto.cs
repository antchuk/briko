﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	/// <summary>
	/// GUID, PartType, UnitId, Param, NewValue
	/// </summary>
	public class SetCommandDto
	{
		public Guid Setup { get; init; }
		public PartType Part { get; init; }
		public int Unitid { get; init; }
		public int Param {  get; init; }
		public float NewValue { get; init; }
	}
}
