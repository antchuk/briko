﻿namespace Services.Contracts
{
	public class StateMachineSettings
	{
		public int TimerInterval { get; set; }
		public string StateHolderAdress { get; set; }
	}
}
