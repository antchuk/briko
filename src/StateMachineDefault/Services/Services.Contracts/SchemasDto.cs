﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class SchemasDto
    {
        public Guid Id { get; init; }
        public Guid Setup { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        //public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> Parts { get; set; }
        public PartType PartType { get; set; }
        public int UnitId { get; set; }
        //public ConcurrentDictionary<int, float> Values { get; set; }
        public int ParamId { get; set; }
        public float ParamValue { get; set; }
    }
}
