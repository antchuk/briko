﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class StateData
	{
		public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> Parameters { get; set; }
		public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>>> ParUpdateTime { get; set; }
	}
}
