﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class QueueServiceNames
	{
		public string QSetGauge { get; set; }
		public string QSetICP7045D { get; set; }
		public string QSetUPC { get; set; }
	}
}
