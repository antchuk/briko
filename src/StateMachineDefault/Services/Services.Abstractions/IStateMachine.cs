﻿using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
	public interface IStateMachine
	{
		//public Task<bool> ShutDownSetup();
		//public Task<bool> AllOnSetup();
		public void SetGuid();
		public Task<bool> UpdateTarget(Guid setup, PartType partType, int unitId, int key, float newValue);
		public bool IsRunning(Guid id);
		//public void StartAskingTimer();
		//public void StopAskingTimer();
		public Task<StateData> GetStateInfo(Guid setupId);

    }
}
