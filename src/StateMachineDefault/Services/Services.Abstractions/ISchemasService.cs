﻿//using Microsoft.EntityFrameworkCore.Migrations.Operations;

using Services.Contracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstractions
{
	public interface ISchemasService
	{

		public Task<List<SchemasDto>> GetSchema(Guid id);
        public Task CreateSchema(Guid setup, PartType partType, int unitId, string name, string desc, int paramId, int paramValue);

    }
}
