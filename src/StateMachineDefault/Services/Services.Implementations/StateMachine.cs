﻿using Services.Implementations.Parts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Net.Http.Headers;
using Services.Implementations.States;
using Services.Abstractions;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Http;
using System.Net.Http;
using System.Reflection.Metadata.Ecma335;
using System.Net.Http.Json;
using System.Text.Json;
using Services.Contracts;
using Microsoft.VisualBasic;
using System.Xml.Linq;
using MassTransit;
using Microsoft.Extensions.Options;
using System.Globalization;
using Services.Repositories.Abstractions;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;


namespace Services.Implementations

{
	public class StateMachine : IStateMachine, IDisposable
	{
		public State _state;
		public Guid Id { get; set; }
		private readonly Microsoft.Extensions.Logging.ILogger<StateMachine> _logger;
		private readonly ICommanderService _commanderService1;
		
		IHttpClientFactory _httpClientFactory;			
		//settings
		private StateMachineSettings _machineSettings;
		private readonly IServiceScopeFactory _scopeFactory;
		public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> Maxs { get; set; }
		public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> Mins { get; set; }
		public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> Targets { get; set; }
		public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> StepPlus { get; set; }
		public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> StepMinus { get; set; }
		private string _name;
		private System.Threading.Timer _timerMain;
		private System.Threading.TimerCallback _timerMainCallBack;
		private static object _locker = new object();
		private bool _killAll;
		private AutoResetEvent _killTimerEvent;
		public ConcurrentDictionary<PartType, SetupPart> Parts { get; set; }

		public StateMachine(Microsoft.Extensions.Logging.ILogger<StateMachine> logger, IHttpClientFactory httpClientFactory,
			ICommanderService commanderService, IOptions<StateMachineSettings> options, IServiceScopeFactory scopeFactory)
		{
			_logger = logger;
			_commanderService1 = commanderService;
			_machineSettings = options.Value;
			_httpClientFactory = httpClientFactory;
			_scopeFactory = scopeFactory;
			_name = "default_setup";
			_killAll = false;
			_timerMainCallBack = new TimerCallback(SendTimerCallBack);
			int _timerInteval = _machineSettings.TimerInterval;			
			_logger.LogInformation($"{DateTime.UtcNow} Initiating setup {_name} started");
			//Parts = new ConcurrentDictionary<PartType, SetupPart>();

			//TODO запросить схему установки (перечень узлов)
			//Parts.TryAdd(PartType.Switcher, new DO7045(_logger, "switcher for gauge", this, PartType.Switcher, 4, true));
			//Parts.TryAdd(PartType.Gauge, new Gauge(_logger, "myGauge", this, PartType.Gauge, 0, false));

			//settings load
			//SettingsIni();

			//старт таймера опроса 
			//if (Id != Guid.Empty)
			//{
				//_timerMain = new Timer(_timerMainCallBack, null, 100, _timerInteval);
				_logger.LogInformation($"{DateTime.UtcNow} Initiating setup {_name} done");
			//}
			//TransitionTo(new SSState());
			
		}
		private async void SettingsIni()
		{
			Maxs = new ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>>();
			Mins = new ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>>();
			Targets = new ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>>();
			StepMinus = new ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>>();
			StepPlus = new ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>>();
			foreach (var part in Parts) 
			{
				Maxs.TryAdd(part.Key, new ConcurrentDictionary<int, ConcurrentDictionary<int, float>> ());
				Maxs[part.Key].TryAdd(part.Value.UnitId, new ConcurrentDictionary<int, float> ());
				Mins.TryAdd(part.Key, new ConcurrentDictionary<int, ConcurrentDictionary<int, float>>());
				Mins[part.Key].TryAdd(part.Value.UnitId, new ConcurrentDictionary<int, float>());
				Targets.TryAdd(part.Key, new ConcurrentDictionary<int, ConcurrentDictionary<int, float>>());
				Targets[part.Key].TryAdd(part.Value.UnitId, new ConcurrentDictionary<int, float>());
			}
			//загрузка значений из репозитория
			/*if (Id != Guid.Empty)
			{
				using (var scope = _scopeFactory.CreateScope())
				{
					var rep = scope.ServiceProvider.GetRequiredService<ISettingsRepository>();
					var _max = await rep.GetSetAsync(this.Id, SettingsType.Maxs);
					var _min = await rep.GetSetAsync(this.Id, SettingsType.Mins);
					var _stepPlus = await rep.GetSetAsync(this.Id, SettingsType.StepPlus);
					var _stepMinus = await rep.GetSetAsync(this.Id, SettingsType.StepMinus);
					FillSetting(_max, Maxs);
					FillSetting(_min, Mins);
					FillSetting(_stepPlus, StepPlus);
					FillSetting(_stepMinus, StepMinus);
				}
			}*/
		}
		private void FillSetting(List<Domain.Entities.Settings> set, ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> dic)
		{		
			if (set != null && dic != null) 
			foreach (var setting in set)
			{
				if (!dic.ContainsKey(setting.PartType))
				{
					dic.TryAdd(setting.PartType, new ConcurrentDictionary<int, ConcurrentDictionary<int, float>>());
				}
				if (!dic[setting.PartType].ContainsKey(setting.UnitId))
				{
					dic[setting.PartType].TryAdd(setting.UnitId, new ConcurrentDictionary<int, float>());
				}
				dic[setting.PartType][setting.UnitId].TryAdd(setting.Parameter, setting.PValue);
			}
		}
		public void SetGuid()
		{
			//todo очистка всего, если меняется ИД??
			//Id = _id;
			//StopAskingTimer();
			//StartAskingTimer();
		}
		public bool IsRunning(Guid _id)
		{
			if (_id == Id && !_killAll)
				return true;
			else
				return false;
		}
		public async Task<bool> UpdateTarget(Guid setup, PartType partType, int unitId, int key, float newValue)
		{
            /*if (Targets == null)
			{
				Targets = new ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> ();
			}					
			if (!Targets.ContainsKey(partType))
			{
				Targets.AddOrUpdate(partType, new ConcurrentDictionary<int, ConcurrentDictionary<int, float>> { }, 
					(key, val) => new ConcurrentDictionary<int, ConcurrentDictionary<int, float>> { });
			}
			if (!Targets[partType].ContainsKey(unitId))
			{
				Targets[partType].AddOrUpdate(unitId, new ConcurrentDictionary<int, float> { },
					(key, val) => new ConcurrentDictionary<int, float> { });
			}
			Targets[partType][unitId].AddOrUpdate(key, newValue, (key, oldval) => newValue);*/
            _commanderService1.SendCommand(setup, partType, unitId, key, newValue);

            return true;
		}
		public void Dispose()
		{
			//остановить все атймеры
			//StopAskingTimer();
			_logger.LogInformation($"{DateTime.UtcNow} Stop State Machine");
		}

		public void LogSome(string message)
		{
			_logger.LogInformation($"{DateTime.UtcNow} {message}");
		}
		private bool StopTimer(System.Threading.Timer _timer, AutoResetEvent _evnt)
		{
			if (_timer != null)
			{
				if (_evnt == null)
				{
					_timer.Dispose();
					return true;
				}
				_evnt.WaitOne();
				if (_timer != null)
				{
					_timer.Dispose();
				}
				return true;
			}
			else return false;
		}
		public void StopAskingTimer()
		{
			_killAll = true;
			if (StopTimer(_timerMain, _killTimerEvent))
			{
				_logger.LogInformation($"{DateTime.UtcNow} timer was killed successfully");
			}
		}
		public void StartAskingTimer()
		{
			if (_killAll)
			{
				_killAll = false;
				//SettingsIni();
				int _timerInteval = _machineSettings.TimerInterval;
				//_timerMain = new Timer(_timerMainCallBack, null, 100, _timerInteval);
				_logger.LogInformation($"{DateTime.UtcNow} timer started");
			}
		}
		private void SendTimerCallBack(object state)
		{
			//проверяем закончился ли предыдущий тик таймера
			if (Monitor.TryEnter(_locker))// && Id!=Guid.Empty)
			{
				try
				{
					//нужно ли останалвиваться?
					if (!_killAll)
					{
						/*var _data = GetStateInfo(Id).Result;
						if (_data != null)
						{
							//обновляем значения параметров
							//UpdateParts(_data);
							//проверка на таймауты: если updatetime было давно, то если модуль критический - перейти в ошибку

							//пытаемся сделать шаг
							//NextStep();
						}*/
					}
					else
					{
						//даем команду, что тик таймера закончился, занчит можно убивать таймер
						_killTimerEvent.Set();
					}
				}
				catch (Exception e)
				{
					_logger.LogError($"Error in SendTimerTick {e}");
				}
				finally
				{
					Monitor.Exit(_locker);
				}
			}
		}
		private async void NextStep()
		{
			foreach (var part in Parts.Values)
			{
				if (part.Data != null)
				{
					foreach (var param in part.Data)
					{
						//Если разрешено изменять значение
						if (part.CanDoStep(param.Key) &&
							Targets[part.Type][part.UnitId].ContainsKey(param.Key) &&
							param.Value!= Targets[part.Type][part.UnitId][param.Key])
						{

							_commanderService1.SendCommand(Id, part.Type, part.UnitId, param.Key, Targets[part.Type][part.UnitId][param.Key]);
						}
						//для тестов ЗАГЛУШКА
						if (param.Key==109)
						{
							_logger.LogInformation($"test value 109 = {param.Value}");
							float _target;
							if (param.Value == 0)
							{
								_target = 1;
							}
							else _target = 0;
                            _commanderService1.SendCommand(Id, part.Type, part.UnitId, param.Key, _target);
                        }
					}
				}
			}
		}		
		public async Task<StateData> GetStateInfo (Guid setupId)
		{
			using HttpClient _client = _httpClientFactory.CreateClient();
			try
			{
				//http://host.docker.internal:8700/StateData/params/
				var res = await _client.GetAsync(
					$"{_machineSettings.StateHolderAdress}{setupId}");
				if (res.IsSuccessStatusCode) 
				{
					//_logger.LogInformation($"{DateTime.UtcNow} we got smth from StateHolder");
					var _data = res.Content.ReadFromJsonAsync<StateData>();

					if (_data.Result.Parameters.Count>0)
					{
						_logger.LogInformation($"{DateTime.UtcNow} we got smth from {_data.Result.Parameters[PartType.Switcher][4][109]}");
					}
					else
					{
						_logger.LogInformation($"{DateTime.UtcNow} we got smth, but null");
					}
					return _data.Result;
				}
			}
			catch (Exception e) 
			{
				_logger.LogInformation("Error getting State Info: {Error}", e);
			}
			return null;
		}
		private void UpdateParts(StateData data)
		{
			//обновить значения
			foreach(var part in Parts)
			{
				//если есть такой тип узла и такой номер узла
				if (data.Parameters.ContainsKey(part.Value.Type) && data.Parameters[part.Value.Type].ContainsKey(part.Value.UnitId))
				{
					if (Parts[part.Value.Type].Data == null)
					{
						Parts[part.Value.Type].Data = new ConcurrentDictionary<int, float> { };
					}
					//обновляем параметры
					foreach (var par in data.Parameters[part.Value.Type][part.Value.UnitId]) 
					{
						Parts[part.Value.Type].Data.AddOrUpdate(par.Key, par.Value, (key, oldval) => par.Value);
						//проверить допустимо ли это значение?
						bool _helth = part.Value.HealthCheck();

						if (!_helth && part.Value.IsCritical) 
						{
							//TransitionTo(new EmState());
							break;
						}
						if (!_helth)
						{
							//TransitionTo(new ErState());
							break;
						}
						
					}
				}
			}
		}
	}
}
