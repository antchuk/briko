﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Implementations;

namespace Services.Implementations.States
{
    public class ErState : State
    {
        public ErState()
        {
            if (_context != null)
            {
                _context.LogSome("Er state created");
            }
		}
        public override void SS_handle()
        {
			_context.LogSome("do nothing");			
		}
        public override void Er_handle()
        {
			_context.LogSome("do nothing");
		}
        public override void Em_handle()
        {
            //выключить все узлы измеряющего типа
            _context.LogSome("Emergency state");
            //перевести всё в состояние максимально-безопасного функционирования			
        }
        public override void ON_handle()
        {
			_context.LogSome("not allowed, solve the problem");
		}
        public override void OFF_handle()
        {
			//выключаем все узлы установки
			foreach (var part in _context.Parts.Values)
			{
				part.TurnOff();
			}
		}
    }
}
