﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Implementations;

namespace Services.Implementations.States
{
    public class SSState : State
    {
        public SSState()
        {
            if (_context != null)
            {
                _context.LogSome("SS state created");
            }
		}
        public override void SS_handle()
        {
			_context.LogSome("do nothing");			
		}
        public override void Er_handle()
        {
			//выключить все узлы измеряющего типа
			_context.LogSome("Error state");
			//StopAllTimers();
			//Log(true, "Debug", true, "SS  - Go to Er");
			//_context.SettingsData.st_Er = _context.GotSettings("Er", _context.FilesPath);
			//_context.TransitionTo(new SSState(_context.SettingsData.st_Er));
		}
        public override void Em_handle()
        {
            //выключить все узлы измеряющего типа
            _context.LogSome("Emergency state");
            //перевести всё в состояние максимально-безопасного функционирования			
        }
        public override void ON_handle()
        {
            //включаем все узлы установки
            foreach(var part in _context.Parts.Values) 
            {
                part.TurnOn();
            }
        }
        public override void OFF_handle()
        {
			//выключаем все узлы установки
			foreach (var part in _context.Parts.Values)
			{
				part.TurnOff();
			}
		}
    }
}
