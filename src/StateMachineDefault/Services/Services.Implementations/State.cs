﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations
{
	public abstract class State
	{
		public StateMachine _context;
		public string Name;		
		public abstract void SS_handle();
		public abstract void Er_handle();
		public abstract void Em_handle();
		public abstract void ON_handle();
		public abstract void OFF_handle();
		/// <summary>
		/// устанвока контекста
		/// </summary>
		/// <param name="context"></param>
		public void SetContext(StateMachine context)
		{
			_context = context;
		}		
	}
}
