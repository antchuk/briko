﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Abstractions;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Threading;
using Services.Contracts;
using MassTransit;
using Microsoft.Extensions.Options;
using MassTransit.Transports;

namespace Services.Implementations
{
	/// <summary>
	/// отправка сообщения в RabbitMq
	/// </summary>
	public class CommanderService : ICommanderService
	{
		private readonly Microsoft.Extensions.Logging.ILogger<CommanderService> _logger;
		private readonly QueueServiceNames _queueNames;
		private readonly IBus _bus;
		public CommanderService(Microsoft.Extensions.Logging.ILogger<CommanderService> logger, IOptions<QueueServiceNames> options, IBus bus)
		{
			_logger = logger;
			_queueNames = options.Value;
			_bus = bus;
		}
		public async Task SendCommand(Guid id, PartType part, int unitId, int param, float newVal)
		{
			string qname = GetQName(part);
			if (qname != null)
			{
				var endpoint = await _bus.GetSendEndpoint(new Uri($"exchange:{qname}"));
				await endpoint.Send(new SetCommandDto
				{
					NewValue = newVal,
					Param = param,
					Unitid = unitId,
					Setup = id,
					Part = part
				});
			}
		}
		private string GetQName(PartType partType)
		{
			switch (partType)
			{
				case PartType.Gauge:
					return _queueNames.QSetGauge;
				case PartType.UPC:
					return _queueNames.QSetUPC;
				case PartType.Switcher:
					return _queueNames.QSetICP7045D;
				default:
					return "";
			}
		}
	}	
}
