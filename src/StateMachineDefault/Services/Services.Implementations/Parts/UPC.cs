﻿using Services.Implementations.Parts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Implementations.Parts
{
	public class UPC : SetupPart
	{
		public UPC(ILogger logger, string name, StateMachine setup, PartType type, int unitid, bool isCritical) : base(logger, name, setup, type, unitid, isCritical)
		{
			_logger.LogInformation($"{DateTime.UtcNow} UPC created");
			this.Type = PartType.UPC;
		}
		public override void TurnOn()
		{
			if (StateOnOff)
			{
				_logger.LogInformation($"{DateTime.UtcNow} UPC already ON");
			}
			else
			{
				StateOnOff = true;
				_logger.LogInformation($"{DateTime.UtcNow} UPC turn ON");
			}
		}
		public override void TurnOff()
		{
			if (StateOnOff)
			{
				StateOnOff = false;
				_logger.LogInformation($"{DateTime.UtcNow} UPC turned OFF");
			}
			else
			{
				_logger.LogInformation($"{DateTime.UtcNow} UPC already OFF");
			}
		}
		public override bool CanDoStep(int param)
		{
			switch (param)
			{
				case 0:
					return false;					

				default: return false;
			}
		}
	}
}
