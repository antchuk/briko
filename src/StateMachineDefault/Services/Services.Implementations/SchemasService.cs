﻿using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Contracts;
using System.Collections.Concurrent;

namespace Services.Implementations
{
    public class SchemasService: ISchemasService
    {
        private readonly Microsoft.Extensions.Logging.ILogger<CommanderService> _logger;
        private readonly ISchemasRepository _schemasRepository;
        public SchemasService(ILogger<CommanderService> logger, ISchemasRepository schemasRepository)
        {
            _logger = logger;
            _schemasRepository = schemasRepository;
        }
        public async Task<List<SchemasDto>> GetSchema(Guid id)
        {
            var res = await _schemasRepository.GetSchemaAsync(id);
            if (res == null) { return null; }
            var ans = new List<SchemasDto>();
            foreach (var r in res)
            {
                ans.Add(new SchemasDto
                {
                    Id = r.Id,
                    Name = r.Name,
                    Description = r.Description,
                    PartType = r.PartType,
                    Setup = r.Setup,
                    UnitId = r.UnitId,
                    ParamId = r.ParamId,
                    ParamValue = r.ParamValue
                });
            }
            return ans;
        }
        public async Task CreateSchema(Guid setup, PartType partType, int unitId,  string name, string desc, int paramId, int paramValue)
        {

            var _newSchema = new Domain.Entities.Schemas
            {
                Setup = setup,
                PartType = partType,
                UnitId = unitId,
                Name = name,
                Description = desc,
                ParamId=paramId,
                ParamValue = paramValue
            };
            _schemasRepository.Add(_newSchema);
            await _schemasRepository.SaveChangesAsync();
        }
    }
}
