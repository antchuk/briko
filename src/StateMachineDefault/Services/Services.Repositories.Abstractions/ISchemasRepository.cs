﻿using Domain.Entities;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
	public interface ISchemasRepository : IRepository<Schemas, Guid>
	{
		Task<List<Schemas>> GetSchemaAsync(Guid setup);
	}
}
