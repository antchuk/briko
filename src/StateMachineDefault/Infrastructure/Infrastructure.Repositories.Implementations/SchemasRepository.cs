﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
	/// <summary>
	/// репозиторий настроек машины
	/// </summary>
	public class SchemasRepository: Repository<Schemas, Guid>, ISchemasRepository
	{
		private readonly DbContext _context;
		private readonly DbSet<Schemas> _schemas;
		public SchemasRepository(DatabaseContext context) : base(context) 
		{
			_schemas = context.Set<Schemas>();
			_context = context;
		}
		public async Task<List<Schemas>> GetSchemaAsync(Guid id)
		{
			//_context.
			var res = await _schemas.Where(x => x.Setup == id).ToListAsync();
            return res;
		}

	}
}
