﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Infrastructure.EntityFramework.Extensions;
using Npgsql;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using System.Collections.Concurrent;

namespace Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {      
        
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureDeletedAsync().Wait();
            //Database.EnsureCreated();
        }
        /// <summary>
        /// схемы установки
        /// </summary>
        public DbSet<Schemas> Schemas { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseIdentityColumns();

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Schemas>().HasKey(x => x.Id);
            //modelBuilder.Entity<Schemas>().OwnsOne(X => X.Values);
            /*modelBuilder.Entity<Schemas>().Property(x => x.Values).HasConversion(
                v => JsonConvert.SerializeObject(v),
                v => JsonConvert.DeserializeObject<ConcurrentDictionary<int, float>>(v));       */        
            //modelBuilder.Entity<Settings>().Property(x => x.Id).ValueGeneratedOnAdd();

			modelBuilder.Seed();
        }
    }
}