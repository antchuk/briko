﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.EntityFramework.Extensions
{
	public static class ModelBuilderExtesions
	{
		public static void Seed(this ModelBuilder modelBuilder)
		{
			for (var i = 0; i < 16; i++) 
			{
                var _newschema = new Schemas
                {
                    Id = Guid.NewGuid(),
                    Setup = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                    Name = $"DO {i}",
                    Description = "ICP DAS digital output",                    
                    PartType = Services.Contracts.PartType.Switcher,
                    UnitId = 4,
                    ParamValue = 0,
                    ParamId = 109+i
                };
                modelBuilder.Entity<Schemas>()
                    .HasData(
                    _newschema);
            }						
		}
	}
}
