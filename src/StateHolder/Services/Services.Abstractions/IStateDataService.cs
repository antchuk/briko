﻿//using Microsoft.EntityFrameworkCore.Migrations.Operations;

using Services.Contracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstractions
{
	public interface IStateDataService
	{
		public Task<bool> UpdateDataOperation(Guid idsetup, int parttype, int idunit, int param, float data);
		public Task<SetupData> GetFullStateData(Guid _id);
        public Task<ConcurrentDictionary<int, float>> GetSingleUnitStateData(Guid _id, int parttype, int idunit);
    }
}
