﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Abstractions;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using Services.Contracts;
using System.Reflection.Metadata.Ecma335;

namespace Services.Implementations
{
	public class StateDataService : IStateDataService
	{
		private ConcurrentDictionary<Guid, SetupData> SetupsData { get; set; }
		//public ConcurrentDictionary<Guid, ConcurrentDictionary<int, DateTime>> ParamsAge { get; set; }
		//private ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> Parameters { get; set; }
		//private ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>>> ParUpdateTime { get; set; }
		//public SetupData SetupData { get; set; }

		private readonly Microsoft.Extensions.Logging.ILogger<StateDataService> _logger;

		public StateDataService(Microsoft.Extensions.Logging.ILogger<StateDataService> logger) 
		{
			_logger = logger;
			//TODO загрузка данных прошлого состояния
			SetupsData = new ConcurrentDictionary<Guid, SetupData> { };
			//Parameters = new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> { };
			//ParUpdateTime = new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>>> { };
		}
		public async Task<bool> UpdateDataOperation(Guid idsetup, int parttype, int idunit, int param, float data)
		{
			if (!SetupsData.ContainsKey(idsetup)) 
			{
				_logger.LogInformation($"no setups with id {idsetup}");
				SetupsData.AddOrUpdate(idsetup, new SetupData(parttype,idunit), (key, oldvalue) => new SetupData(parttype,idunit));				
			}
			if (!SetupsData[idsetup].Parameters.ContainsKey(parttype)) 
				SetupsData[idsetup].Parameters.AddOrUpdate(parttype, new ConcurrentDictionary<int, ConcurrentDictionary<int, float>> { }, (key, value) => new ConcurrentDictionary<int, ConcurrentDictionary<int, float>> { });
			if (!SetupsData[idsetup].Parameters[parttype].ContainsKey(idunit))
				SetupsData[idsetup].Parameters[parttype].AddOrUpdate(idunit, new ConcurrentDictionary<int, float>{ }, (key, val) => new ConcurrentDictionary<int, float> { });
			SetupsData[idsetup].Parameters[parttype][idunit].AddOrUpdate(param, data, (key, val) => data);

			if (!SetupsData[idsetup].ParUpdateTime.ContainsKey(parttype))
				SetupsData[idsetup].ParUpdateTime.AddOrUpdate(parttype, new ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>> { }, (key, value) => new ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>> { });
			if (!SetupsData[idsetup].ParUpdateTime[parttype].ContainsKey(idunit))
				SetupsData[idsetup].ParUpdateTime[parttype].AddOrUpdate(idunit, new ConcurrentDictionary<int, DateTime> { }, (key, val) => new ConcurrentDictionary<int, DateTime> { });
			SetupsData[idsetup].ParUpdateTime[parttype][idunit].AddOrUpdate(param, DateTime.UtcNow, (key, val) => DateTime.UtcNow);

			return true;			

		}
		public async Task<SetupData> GetFullStateData(Guid _id)
		{
			SetupData ans = new();
			if (!SetupsData.ContainsKey(_id))
			{
				_logger.LogInformation($"no setups with id {_id}");
				return ans;
			}

			return SetupsData[_id];
		}
        public async Task<ConcurrentDictionary<int, float>> GetSingleUnitStateData(Guid _id, int parttype, int unitid)
		{
			if (SetupsData.ContainsKey(_id) &&
				SetupsData[_id].Parameters.ContainsKey(parttype) &&
				SetupsData[_id].Parameters[parttype].ContainsKey(unitid))
			{
				var _ans = SetupsData[_id].Parameters[parttype][unitid];
                return _ans;
            }
			return new ConcurrentDictionary<int, float> ();
		}
    }
}
