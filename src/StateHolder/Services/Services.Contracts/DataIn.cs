﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    /// <summary>
	/// модель данных для входящего сообщения из очереди на парсинг
	/// </summary>
	public class DataIn
    {
        /// <summary>
        /// клиент
        /// </summary>
        public Guid IDSetup { get; set; }
        /// <summary>
        /// номер парсера
        /// </summary>
        public int IDPartType { get; set; }
        /// <summary>
        /// номер ухла (если их неколько)
        /// </summary>
        public int IDUnit { get; set; }
        /// <summary>
        /// данные для парсинга
        /// </summary>
        public string Data { get; set; }

    }
}
