﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class ICPDto
	{
		public Dictionary<int, float> Data { get; init; }
		public bool succed { get; init; }
		public Guid IDSetup { get; init; }
		public int IDPartType { get; init; }
		public int IDUnit { get; init; }
	}
}

