﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class SetupData
	{
		/// <summary>
		/// ключи первый/второй/третий - тип узлов/номер юнита/номер апарметра - параметры
		/// </summary>
		public ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int , float>>> Parameters { get; set; }
		public ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>>> ParUpdateTime { get; set; }
		public SetupData(int parttype, int unit) 
		{
			Parameters = new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> { };
			ParUpdateTime = new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>>> { };
			Parameters.TryAdd(parttype, new ConcurrentDictionary<int, ConcurrentDictionary<int, float>> {});
			Parameters[parttype].TryAdd(unit, new ConcurrentDictionary<int, float> { });
			Parameters[parttype][unit] = new ConcurrentDictionary<int, float> { };
		}
		public SetupData()
		{
			Parameters = new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> { };
			ParUpdateTime = new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>>> { };
		}
	}
}
