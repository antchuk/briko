﻿using System;
using System.Collections.Concurrent;

namespace WebApi.Models
{
    /// <summary>
    /// Уведомление
    /// </summary>
    public class StateModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid IdClient { get; init; }
        /// <summary>
        /// состояние всех параметров
        /// </summary>
        public ConcurrentDictionary<int, float> Data { get; init; }
        /// <summary>
        /// дата последнего обновления
        /// </summary>
		public ConcurrentDictionary<int, DateTime> DataAge { get; init; }
	}

}
