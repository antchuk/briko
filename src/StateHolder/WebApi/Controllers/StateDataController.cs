﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер состояния о установке
    /// </summary>
    [ApiController]
    [Route("StateData")]
    public class StateDataController : ControllerBase
    {
        private readonly IStateDataService _service;
        public StateDataController(IStateDataService service)
        {
            _service = service;
            //_mapper = mapper;
        }
        /// <summary>
        /// Получить все данные для узла ИД
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("params/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            if (id ==  Guid.Empty) 
            {
                return BadRequest();
            }
            var enity = await _service.GetFullStateData(id);
            if (enity == null) 
            {
                return NotFound();
            }
            return Ok(enity);
        }
        /// <summary>
        /// Получить данные только одного определенного устройства
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parttype"></param>
        /// <param name="unitId"></param>
        /// <returns></returns>
        [HttpGet("unitparams/{id:guid}/{parttype:int}/{unitId:int}")]
        public async Task<IActionResult> GetUnitParams(Guid id, int parttype, int unitId)
        {
            if (id == Guid.Empty)
            {
                return BadRequest();
            }
            var enity = await _service.GetSingleUnitStateData(id,parttype,unitId);
            if (enity == null)
            {
                return NotFound();
            }
            if (enity.Count == 0)
            {
                return NoContent();
            }
            return Ok(enity);
        }
    }
}
