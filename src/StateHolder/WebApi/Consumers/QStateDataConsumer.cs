﻿using AutoMapper;
using MassTransit;
using Services.Abstractions;
using Services.Contracts;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System;
using WebApi.Models;
using MassTransit.Configuration;

namespace WebApi.Consumers
{
	/// <summary>
	/// Получатель сообщений
	/// </summary>
	public class QStateDataConsumer : IConsumer<Services.Contracts.UPCdto>, IConsumer<Services.Contracts.ICPDto>
	{
		private readonly IStateDataService _stateDataService;
		private readonly Microsoft.Extensions.Logging.ILogger<QStateDataConsumer> _logger;

		public QStateDataConsumer( IStateDataService stateDataService,
			Microsoft.Extensions.Logging.ILogger<QStateDataConsumer> logger)
		{
			_stateDataService = stateDataService;
			_logger = logger;
		}
		public async Task Consume(ConsumeContext<Services.Contracts.UPCdto> context)
		{
			_logger.LogInformation($"we have got message from UPC: {context.Message.succed}");
			if (context == null)
			{
				//послать сообщение об ошибке в очередь ошибок?
				_logger.LogInformation("null context");
				return;
			}
			if (context.Message.IDSetup == Guid.Empty)
			{
				//послать сообщение об ошибке в очередь ошибок?
				_logger.LogInformation("empty guid");
				return;
			}
			if (context.Message.IDPartType == 0)
			{
				//послать сообщение об ошибке в очередь ошибок?
				_logger.LogInformation("zero paramID");
				return;
			}
			if (context.Message.succed)
			{
				foreach (var t in context.Message.Data) 
				{
					await _stateDataService.UpdateDataOperation(context.Message.IDSetup, context.Message.IDPartType, context.Message.IDUnit, t.Key, t.Value);
					_logger.LogInformation($"logged {t.Value}");
				}
			}
		}
		public async Task Consume(ConsumeContext<Services.Contracts.ICPDto> context)
		{
			_logger.LogInformation($"we have got message from ICP: {context.Message.succed}");
			if (context == null)
			{
				//послать сообщение об ошибке в очередь ошибок?
				_logger.LogInformation("null context");
				return;
			}
			if (context.Message.IDSetup == Guid.Empty)
			{
				//послать сообщение об ошибке в очередь ошибок?
				_logger.LogInformation("empty guid");
				return;
			}
			if (context.Message.IDPartType == 0)
			{
				//послать сообщение об ошибке в очередь ошибок?
				_logger.LogInformation("zero paramID");
				return;
			}
			if (context.Message.succed)
			{
				foreach (var t in context.Message.Data)
				{
					await _stateDataService.UpdateDataOperation(context.Message.IDSetup, context.Message.IDPartType, context.Message.IDUnit, t.Key, t.Value);
					_logger.LogInformation($"logged {t.Value}");
				}
			}
		}
	}
	/// <summary>
	/// не создает дополнителные Exchange/queue в RabbitMQ
	/// </summary>
	public class QStateDataDefinition : ConsumerDefinition<QStateDataConsumer>
	{
		protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<QStateDataConsumer> consumerConfigurator)
		{
			endpointConfigurator.ConfigureConsumeTopology = false; //не создается Exchange
		}
	}
}
