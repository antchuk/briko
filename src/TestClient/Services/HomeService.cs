﻿using System.Collections.Concurrent;
using TestClient.Models.HardwareMachine;
using TestClient.Providers.Home;

namespace TestClient.Services
{
	public class HomeService : IHomeSerice
	{
		private readonly IHttpContextAccessor _contextAccessor;
		private readonly IHomeProvider _homeProvider;
		public HomeService(IHttpContextAccessor contextAccessor, IHomeProvider homeProvider)
		{
			_contextAccessor = contextAccessor;
			_homeProvider = homeProvider;
		}
		public async Task<bool> StartPolling(string message, CancellationToken token = default)
		{
			_ = await _homeProvider.StartPolling(message, token);
			return true;
		}
        /*public async Task<bool> SendMessage(string message, Guid setupId, PartType partType, int unitId, CancellationToken token = default)
        {
            //_ = await _homeProvider.SendMessage(message, setupId, partType, unitId, token);
            return true;
        }*/
		public async Task<ConcurrentDictionary<int, float>> GetSwitcherState(Guid setupId, PartType partType, int unitId, CancellationToken token = default)
		{
			var res = await _homeProvider.GetSwitcherState(setupId, partType, unitId, token);
			//сложный тип нужен новый гет
			return res;
		}
    }
}
