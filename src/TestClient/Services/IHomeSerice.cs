﻿using System.Collections.Concurrent;
using TestClient.Models.HardwareMachine;

namespace TestClient.Services
{
	public interface IHomeSerice
	{
		Task<bool> StartPolling(string message, CancellationToken token = default);

        //Task<bool> SendMessage(string message, Guid setupId, PartType partType, int unitId, CancellationToken token = default);
		Task<ConcurrentDictionary<int, float>> GetSwitcherState(Guid setupId, PartType partType, int unitId, CancellationToken token = default);
    }
}
