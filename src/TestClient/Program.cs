using TestClient.Models.Common;
using TestClient.Providers.Home;
using TestClient.Services;
using Microsoft.Extensions.Configuration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddHttpClient(Constants.Home_HTTP_CLIENT_NAME, c =>
{
	c.DefaultRequestHeaders.Add("Accept", "application/json; charset=UTF-8");
	c.BaseAddress = new Uri("http://localhost:8770/");
	//c.BaseAddress = new Uri(jwtSettings.Issuer);
	//c.Timeout = TimeSpan.FromSeconds(30);
});
builder.Services.AddHttpClient(Constants.State_HTTP_CLIENT_NAME, c =>
{
    c.DefaultRequestHeaders.Add("Accept", "application/json; charset=UTF-8");
    c.BaseAddress = new Uri("http://localhost:8700/");
    //c.BaseAddress = new Uri(jwtSettings.Issuer);
    //c.Timeout = TimeSpan.FromSeconds(30);
});
builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IHomeSerice,HomeService>();
builder.Services.AddScoped<IHomeProvider,HomeProvider>();

//builder.Services.AddSingleton<IHardwareAskMachine,HardwareAskMachine>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Home/Error");
	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
	app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
	name: "default",
	pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
