﻿const dragNDrop = () => {
    const dragging = document.querySelectorAll('.unit-js');
    const containers = document.querySelectorAll('.js-container');
    const dragStart = function (e) {

        e.currentTarget.style.border = "dashed";
        e.dataTransfer.effectAllowed = 'copy';
        e.dataTransfer.setData("text", e.target.id);
    };
    const dragEnd = function (ev) {
        ev.target.style.border = "1px solid";
        
    };
    const dragOver = function (evt) {        
        evt.preventDefault();                    
    };
    const dragEnter = function (evt) {       
        evt.preventDefault();
        //console.log(evt.currentTarget.childElementCount);
        evt.currentTarget.style.border = "1px dashed red";
               
    };
    const dragLeave = function (ev) {
        ev.target.style.border = "none";
    };
    const dragDrop = function (ev) {
        ev.preventDefault();
        var id = ev.dataTransfer.getData("text");
        var nodeCopy = document.getElementById(id).cloneNode(true);
        nodeCopy.id = "draggedId";
        nodeCopy.style.border = "none";
        ev.target.replaceChildren();
        ev.target.appendChild(nodeCopy);
        ev.target.style.border = "none";
    };

    dragging.forEach((dr) => {
        dr.addEventListener('dragstart', dragStart);
        dr.addEventListener('dragend', dragEnd);
    });
    containers.forEach((cell) => {
        cell.addEventListener('dragover', dragOver);
        cell.addEventListener('dragenter', dragEnter);
        cell.addEventListener('dragleave', dragLeave);
        cell.addEventListener('drop', dragDrop);
    });
};
dragNDrop();

