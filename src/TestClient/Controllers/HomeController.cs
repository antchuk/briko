using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using TestClient.Models;
using TestClient.Models.HardwareMachine;
using TestClient.Services;

namespace TestClient.Controllers
{
	//[ApiController]
	//[Route("recievecommand")]
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;
		private readonly IHomeSerice _homeSerice;
		//private readonly IHardwareAskMachine _hardwareAskMachine;	
		private List<SwitcherTestModel> _switcherModels;
        private readonly IConfiguration _configuration;
        public HomeController(ILogger<HomeController> logger, IHomeSerice homeSerice, IConfiguration configuration)
		{
			_logger = logger;
			_homeSerice = homeSerice;
			_configuration = configuration;
            _switcherModels = new List<SwitcherTestModel>();
        }

		public async Task<IActionResult> Index()
		{
			_switcherModels.Clear();
			//_switcherModels.Add(new SwitcherTestModel(1,0));
			//_switcherModels.Add(new SwitcherTestModel(2,0));
			//_switcherModels.Add(new SwitcherTestModel(3,1));
			Guid my = Guid.Parse(_configuration["SetupID"]);

            var res = await _homeSerice.GetSwitcherState(Guid.Parse(_configuration["SetupID"]), PartType.Switcher, 4);
            ViewBag.Switchers = res;
            //ViewBag.SwitchersIndex = res.Keys;
            //ViewBag.SwitchersValues = res.Values;
            return View();
		}


		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
		public async Task<IActionResult> StartPolling(SwitcherTestModel model, CancellationToken cancellationToken)
		{
			//_ = int.TryParse(model.Count, out int _count);
			for (int i = 0;i<1000; i++)
			{
				await _homeSerice.StartPolling("200", cancellationToken);
			}			
			return View(nameof(Index), model);
		}

        [HttpPost]
		public async Task<IActionResult> Recievecommand([FromBody] Guid setup, PartType partType, int unitId, string command)
		{


			return Ok();
		}
    }
}
