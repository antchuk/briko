﻿namespace TestClient.Models.HardwareMachine
{
    public enum PartType
    {
        Gauge,
        UPC,
        Switcher,
        DO7045,
        Chamber,
        TurbomolecularPump,
        ScrollPump



    }
}
