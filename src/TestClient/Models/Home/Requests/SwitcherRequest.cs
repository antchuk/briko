﻿namespace TestClient.Models.Home.Requests
{
    public class SwitcherRequest
    {
        public List<int> channels { get; set; }
    }
}
