﻿namespace TestClient.Models.Common
{
	public class Constants
	{
		/// <summary>
		/// Название http клиента провайдера Home
		/// </summary>
		public const string Home_HTTP_CLIENT_NAME = "HOME_HTTP_CLIENT";
        public const string State_HTTP_CLIENT_NAME = "State_HTTP_CLIENT";
    }
}
