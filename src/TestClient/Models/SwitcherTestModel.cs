﻿using System.ComponentModel.DataAnnotations;

namespace TestClient.Models
{
	public class SwitcherTestModel
	{
		public int Number { get; set; }
		public float Value { get; set; }
		public SwitcherTestModel(int i, float val) 
		{
			Number = i;
			Value = val;
		}
        public SwitcherTestModel()
        {
        }
    }
}
