﻿using System.Collections.Concurrent;
using TestClient.Models.HardwareMachine;
using TestClient.Models.Home.Requests;

namespace TestClient.Providers.Home
{
	public interface IHomeProvider
	{
		Task<bool> StartPolling(string message, CancellationToken cancellationToken = default);
		//Task<bool> SendMessage (string message, Guid setupId, PartType partType, int unitId, CancellationToken cancellationToken = default);
		Task<ConcurrentDictionary<int, float>> GetSwitcherState(Guid setupId, PartType partType, int unitId, CancellationToken cancellationToken = default);

    }
}
