﻿using System.Collections.Concurrent;
using System.Diagnostics.Eventing.Reader;
using TestClient.Models.Common;
using TestClient.Models.HardwareMachine;
using TestClient.Models.Home.Requests;

namespace TestClient.Providers.Home
{
    public class HomeProvider : ProviderBase, IHomeProvider
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public HomeProvider(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
		public async Task<bool> StartPolling(string message, CancellationToken cancellationToken = default)
        {
            /*var _request = new UPCmessageRequest(Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"), 1, $"({message}.0 000.0 214.0 004 50.0 0100 50.0 00001001");

			_ = await PostAsync<UPCmessageRequest, UPCmessageResponse>(
                _httpClientFactory,
                Constants.Home_HTTP_CLIENT_NAME,
                _request,
                $"datareciever",
                cancellationToken);*/
            return true;
        }
        /*public async Task<bool> SendMessage(string message, Guid setupId, PartType partType, int unitId, CancellationToken cancellationToken = default)
        {
            var _request = new UPCmessageRequest(setupId, partType, unitId, 0, message);

            _ = await PostAsync<UPCmessageRequest, UPCmessageResponse>(
                _httpClientFactory,
                Constants.Home_HTTP_CLIENT_NAME,
                _request,
                $"datareciever",
                cancellationToken);
            return true;
        }*/
        public async Task<ConcurrentDictionary<int, float>> GetSwitcherState(Guid setupId, PartType partType, int unitId, CancellationToken cancellationToken = default)
        {
            //поменять запрос на новый гет
            var resp = await GetAsync<ConcurrentDictionary<int,float>>(
                _httpClientFactory,
                Constants.State_HTTP_CLIENT_NAME,                
                $"StateData/unitparams/{setupId}/{(int)partType}/{unitId}",//guid вписать? //
                cancellationToken);
            
            return resp;
        }
    }
}
