﻿using System.Collections.Concurrent;

namespace TestClientBlazorWAS.Models
{
    public class SetupPartModel
    {
        public ConcurrentDictionary<int , float> Data { get; set; }
        public ConcurrentDictionary<int,string> ParamsNames { get; set; }
        public UnitStatuses Status { get; set; }
        public PartType PartType { get; set; }
        public int unitId { get; set; }
        public DateTime LastUpdated { get; set; }

    }
}
