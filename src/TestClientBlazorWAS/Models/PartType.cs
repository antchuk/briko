﻿namespace TestClientBlazorWAS.Models
{
    public enum PartType
    {
        Gauge,
        UPC,
        Switcher,
        DO7045,
        Chamber,
        TurbomolecularPump,
        ScrollPump
    }
}
