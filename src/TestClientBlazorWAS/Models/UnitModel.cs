﻿namespace TestClientBlazorWAS.Models
{
    public class UnitModel
    {
        public int Id { get; set; }
        public UnitStatuses Status { get; set; }
        public PartType PartType { get; set; }
        public DateTime LastUpdated { get; set; }
        //public int PramCount { get; set; }
    }
}
