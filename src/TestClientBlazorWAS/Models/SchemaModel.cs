﻿namespace TestClientBlazorWAS.Models
{
    public class SchemaModel
    {
        public Guid Id { get; init; }
        public Guid Setup { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public PartType PartType { get; set; }
        public int UnitId { get; set; }
        public int ParamId { get; set; }
        public float ParamValue { get; set; }
    }
}
