﻿namespace TestClientBlazorWAS.Models
{
    public class ParamDto
    {
        public int param {  get; set; }
        public int UnitId { get; set; }
        public PartType PartType { get; set; }
    }
}
