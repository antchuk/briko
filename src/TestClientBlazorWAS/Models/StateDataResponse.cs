﻿using System.Collections.Concurrent;

namespace TestClientBlazorWAS.Models
{
    public class StateDataResponse
    {
        public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, float>>> Parameters { get; set; }
        public ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, DateTime>>> ParUpdateTime { get; set; }

        public float GetValue(PartType partType, int unitId, int param)
        {
            float ans = -999;
            if (Parameters.ContainsKey(partType) &&
                Parameters[partType].ContainsKey(unitId) &&
                Parameters[partType][unitId].ContainsKey(param))
            {
                ans = Parameters[partType][unitId][param];
            }
            return ans;
        }
    }
}
