﻿using System.Collections.Concurrent;

namespace TestClientBlazorWAS.Models
{
    public class SetupModel
    {
        public string Name;
        public Guid Id;
        public string Description;
        public List<SetupPartModel> Parts {  get; set; } = new List<SetupPartModel> { };
        public bool ContainsPart(PartType partType, int unitId)
        {
            if (Parts!=null && Parts.Count>0)
            {
                foreach (var part in Parts) 
                {
                    if (part.PartType == partType && part.unitId == unitId)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public SetupPartModel GetPart(PartType partType, int unitId)
        {
            if (Parts != null && Parts.Count > 0)
            {
                foreach (var part in Parts)
                {
                    if (part.PartType == partType && part.unitId == unitId)
                    {
                        return part;
                    }
                }
            }
            return null;
        }
        public bool ContainsParam(PartType partType, int unitId, int param)
        {
            var _curPart = GetPart(partType, unitId);
            if (_curPart!=null && _curPart.Data!=null && _curPart.Data.Count>0)
            {
                foreach (int p in  _curPart.Data.Keys) 
                {
                    if (p == param) return true;
                }
            }
            return false;
        }
    }
}
