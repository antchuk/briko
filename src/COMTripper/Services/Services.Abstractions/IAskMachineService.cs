﻿using Services.Contracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstractions
{
	public interface IAskMachineService
	{
        Task<bool> StartMachine();
        Task<bool> StopMachine();
        void AddCommand(PartType partType, int unitId, int idCommand, string _cc);
    }
}
