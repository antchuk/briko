﻿using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    public interface ICommunicateService
    {
        Task<string> SendMessage(string message, Guid setupId, PartType partType, int unitId, CancellationToken token = default);
        Task StartStateMAchine();
    }
}
