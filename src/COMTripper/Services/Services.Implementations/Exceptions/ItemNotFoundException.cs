﻿//using Microsoft.AspNetCore.
using System;

namespace Services.Implementations;

public class ItemNotFoundException : HttpException
{
    private const string MESSAGE = "Элемент {0} с Id {1} не найден в базе";

    public ItemNotFoundException(Guid id, string itemName)
        : base(
              httpStatusCode: System.Net.HttpStatusCode.NotFound, //StatusCodes.Status404NotFound, Net7 отключили Microsoft.AspNetCore.Http ???
              message: string.Format(MESSAGE, itemName, id))
    {
    }
}
