﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Services.Abstractions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO.Ports;
using Services.Contracts;
using System.Windows.Input;

namespace Services.Implementations
{
    public class AskMachineService : IAskMachineService
    {
        private readonly Microsoft.Extensions.Logging.ILogger<IAskMachineService> _logger;
        private readonly IConfiguration _configuration;
        private readonly IServiceScopeFactory _scopeFactory;
        public Guid Id { get; set; }
        private StateData _statedata;
        private bool KillAll = false;
        private int MaxErrorCount; //заменить на словарь по параметрам
        private object[] _lockers;
        private int[] _ErrorCount;
        private bool[] _IsPrevDone;
        private bool[] _IsFirstLoop;
        private AutoResetEvent[] _CloseTimerEvents;
        private ConcurrentDictionary<int, System.Threading.Timer> _Timers;
        private ConcurrentDictionary<int, System.Threading.TimerCallback> _TmrCallBacks;
        private ConcurrentDictionary<int, SerialPort> _COMPorts;
        private ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, string>>> WriteCommands;

        public AskMachineService(Microsoft.Extensions.Logging.ILogger<IAskMachineService> logger, IConfiguration configuration, IServiceScopeFactory scopeFactory)
        {
            _logger = logger;
            _configuration = configuration;
            _scopeFactory = scopeFactory;
            //загрузить настройки и списки комманд
            string path = System.IO.Directory.GetCurrentDirectory() + _configuration["SettingsPath"];
            WriteCommands = new ConcurrentDictionary<PartType, ConcurrentDictionary<int, ConcurrentDictionary<int, string>>>();
            //Log(false, false, $"curdir = {System.IO.Directory.GetCurrentDirectory()}");

            if (path != null && !LoadAllSettings(path))
            {
                throw new ArgumentException("Не удалось загрузить настроки состояний");
            }
            Id = Guid.Parse(_configuration["SetupID"]);
            //запуск стайтмашины на сервере
            using (var scope = _scopeFactory.CreateScope())
            {
                var serv = scope.ServiceProvider.GetRequiredService<ICommunicateService>();
                serv.StartStateMAchine();
            }
            Log(false, false, $"HardwareAskMachine started with guid = {Id}");
        }
        /// <summary>
        /// распаковка фала настроек всех состояний
        /// </summary>
        /// <param name="_path"></param>
        /// <returns></returns>
        private bool LoadAllSettings(string _path)
        {
            //SM_data SM_all_data = new();
            try
            {
                _statedata = DeSerialize(_path);
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error in LoadAllSettings " + ex.ToString());
                return false;
            }
            return true;
        }
        /// <summary>
        /// Десериализуем настройки одного состояния
        /// </summary>
        /// <param name="_path"></param>
        /// <param name="_name"></param>
        /// <returns></returns>
        private StateData DeSerialize(string _path)
        {
            StateData data = new();
            using (StreamReader file = File.OpenText(_path))
            {
                Newtonsoft.Json.JsonSerializer ser = new()
                {
                    Formatting = Newtonsoft.Json.Formatting.Indented
                };
                data = (StateData)ser.Deserialize(file, typeof(StateData));
            }
            return data;
        }
        public async Task<bool> StartMachine()
        {
            KillAll = false;
            if (_statedata.ComandsList != null)
            {
                Log(false, false, "HardwareAskMachine asking started");
                RunAllTimers();
            }
            return true;
        }
        public async Task<bool> StopMachine()
        {
            KillAll = true;
            for (int i = 0; i < _Timers.Count; i++)
            {
                StopTimer(_Timers[i], _CloseTimerEvents[i]);
                _logger.LogInformation($"{DateTime.UtcNow} timer {i} was killed successfully");
            }
            foreach (var port in _COMPorts)
            {
                port.Value.Close();
            }

            return true;
        }
        private bool StopTimer(System.Threading.Timer _timer, AutoResetEvent _evnt)
        {
            if (_timer != null)
            {
                if (_evnt == null)
                {
                    _timer.Dispose();
                    return true;
                }
                _evnt.WaitOne();
                if (_timer != null)
                {
                    _timer.Dispose();
                }
                return true;
            }
            else return false;
        }
        private void InitAll(int _count)
        {
            _Timers = new ConcurrentDictionary<int, System.Threading.Timer>();
            _COMPorts = new ConcurrentDictionary<int, SerialPort>();
            _TmrCallBacks = new ConcurrentDictionary<int, TimerCallback>();
            //иницировать локеры
            _lockers = IniSMTH(_count);
            _ErrorCount = IniSMTH(_count, 0);
            _IsPrevDone = IniSMTH(_count, true);
            _IsFirstLoop = IniSMTH(_count, true);
            //_IniComandsCount = IniSMTH(_count, 0);
            //иницировать резет эвенты
            _CloseTimerEvents = IniARE(_count, true);
            Log(false, false, "intiation complited");
        }
        private void Log(bool ToLocalFile, bool ToTCP, string Message)
        {
            //лог консоль
            _logger.LogInformation(Message);
            //лог в файл
            //лог в TCP (отдельный сервис?)
        }
        /// <summary>
        /// настраиваем порт
        /// </summary>
        /// <param name="_Portsett"></param>
        /// <returns></returns>
        private SerialPort IniCOMPort(ComPort _Portsett)
        {
            SerialPort port = new SerialPort(_Portsett.Name, _Portsett.Baudrate);
            //ЗАГЛУШКА с определение четности и стоп битов
            if (_Portsett.Parity == 0)
            {
                port.Parity = Parity.None;
            }
            if (_Portsett.Parity == 1)
            {
                port.Parity = Parity.Odd;
            }
            if (_Portsett.Stopbits == 1)
            {
                port.StopBits = StopBits.One;
            }
            port.DataBits = _Portsett.Databits;
            port.ReadTimeout = _Portsett.Readtimeout;
            port.WriteTimeout = _Portsett.Writetimeout;
            if (_Portsett.EndLine != null) port.NewLine = _Portsett.EndLine;
            port.DtrEnable = _Portsett.DtrEnable;
            port.RtsEnable = _Portsett.RtsEnable;
            return port;
        }
        /// <summary>
        /// попытка открытия порта, false - не смогли
        /// </summary>
        /// <param name="_Numer"></param>
        /// <param name="_settings"></param>
        /// <returns></returns>
        private bool OpenPort(int _Numer, ComPort _settings)
        {
            _COMPorts.AddOrUpdate(_Numer, IniCOMPort(_settings), (key, oldvalue) => IniCOMPort(_settings));
            bool res = true;
            try
            {
                _COMPorts[_Numer].Open();
            }
            catch (Exception ex)
            {
                Log(true, false, " no COMPORT" + _settings.Name);
                res = false;
            }
            return res;
        }
        /// <summary>
        /// инициация массива object
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private object[] IniSMTH(int count)
        {
            object[] ans = new object[count];
            for (int i = 0; i < count; i++)
            {
                ans[i] = new object();
            }
            return ans;
        }
        /// <summary>
        /// инициирует массива bool
        /// </summary>
        /// <param name="count"></param>
        /// <param name="_StartVal"></param>
        /// <returns></returns>
        private bool[] IniSMTH(int count, bool _StartVal)
        {
            bool[] ans = new bool[count];
            for (int i = 0; i < count; i++)
            {
                ans[i] = _StartVal;
            }
            return ans;
        }
        /// <summary>
        /// инициирует массив int
        /// </summary>
        /// <param name="count"></param>
        /// <param name="_StartVal"></param>
        /// <returns></returns>
        private int[] IniSMTH(int count, int _StartVal)
        {
            int[] ans = new int[count];
            for (int i = 0; i < count; i++)
            {
                ans[i] = _StartVal;
            }
            return ans;
        }
        /// <summary>
        /// инициация эвентов для ожидания закрытия таймеров
        /// </summary>
        /// <param name="count"></param>
        /// <param name="_StartVal"></param>
        /// <returns></returns>
        private AutoResetEvent[] IniARE(int count, bool _StartVal)
        {
            AutoResetEvent[] ans = new AutoResetEvent[count];
            for (int i = 0; i < count; i++)
            {
                ans[i] = new AutoResetEvent(_StartVal);
            }
            return ans;
        }
        /// <summary>
        /// запуск всех таймеров
        /// </summary>
        public void RunAllTimers()
        {
            //инициировать всё
            InitAll(_statedata.ComandsList.Count);
            for (int i = 0; i < _statedata.COMTmrSettings.Count; i++)
            {
                if (_statedata.ComandsList[i] != null && _statedata.COMTmrSettings[i].period > 0)
                {
                    switch (_statedata.COMTmrSettings[i].Type)
                    {
                        case 1:
                            //ASCII
                            RunASCIITimer(i, _statedata.COMTmrSettings[i], _statedata.ComandsList[i]);
                            break;
                        case 2:
                            //ModBus TCP
                            RunModBusTCPTimer(i, _statedata.COMTmrSettings[i], _statedata.ComandsList[i], 502, "172.16.0.77");
                            break;
                        case 3:
                            //Modbus RTU

                            break;
                    }
                }
            }
        }
        /// <summary>
        /// запускаме один таймер, 1 - ASCII
        /// </summary>
        /// <param name="_Num"></param>
        /// <param name="_set"></param>
        /// <param name="_list"></param>
        private void RunASCIITimer(int _Num, COMTimer_settings _set, List<Comand_COM> _list)
        {
            if (OpenPort(_Num, _statedata.COMSettings[_Num]))
            {
                _TmrCallBacks.AddOrUpdate(_Num, new TimerCallback(TimerTick), (key, oldvalue) => new TimerCallback(TimerTick));
                TimerData _timerData = new()
                {
                    TypeOfTimer = _set.Type,
                    Numer = _Num,
                    CList = _list,
                    COMPort = _COMPorts[_Num]
                };
                _Timers.AddOrUpdate(_Num, new System.Threading.Timer(_TmrCallBacks[_Num], _timerData, _set.start_delay, _set.period), (key, oldvalue) => new System.Threading.Timer(_TmrCallBacks[_Num], _timerData, _set.start_delay, _set.period));
            }
        }
        /// <summary>
        /// запускаме один таймер, 2 - ModBusTCP
        /// </summary>
        /// <param name="_Num"></param>
        /// <param name="_set"></param>
        /// <param name="_list"></param>
        private void RunModBusTCPTimer(int _Num, COMTimer_settings _set, List<Comand_COM> _list, int TCPport, string _IP)
        {
            _TmrCallBacks.AddOrUpdate(_Num, new TimerCallback(TimerTick), (key, oldvalue) => new TimerCallback(TimerTick));
            TimerData _timerData = new()
            {
                TypeOfTimer = _set.Type,
                Numer = _Num,
                CList = _list,
                IP = _IP,
                port = TCPport
            };
            _Timers.AddOrUpdate(_Num, new System.Threading.Timer(_TmrCallBacks[_Num], _timerData, _set.start_delay, _set.period), (key, oldvalue) => new System.Threading.Timer(_TmrCallBacks[_Num], _timerData, _set.start_delay, _set.period));
        }
        /// <summary>
        /// тик таймера
        /// </summary>
        /// <param name="state"></param>
        private void TimerTick(object state)
        {
            TimerData _data = (TimerData)state;
            if (Monitor.TryEnter(_lockers[_data.Numer]))
            {
                try
                {
                    if (!KillAll)
                    {
                        SelectTypeOfPort(_data);
                    }
                    else
                    {
                        _CloseTimerEvents[_data.Numer].Set();
                    }
                }
                catch (Exception e)
                {
                    Log(true, true, $"exeption timer \n + {e}");
                }
                finally
                {
                    Monitor.Exit(_lockers[_data.Numer]);
                }
            }
        }
        private void SelectTypeOfPort(TimerData _data)
        {
            switch (_data.TypeOfTimer)
            {
                case 1:
                    //ASCII
                    CommandPolling(_data.COMPort, _data.CList, _data.Numer, _IsFirstLoop[_data.Numer]);
                    _IsFirstLoop[_data.Numer] = false;
                    break;
                case 2:
                    //MODBUS TCP
                    CommandPolling(_data, _IsFirstLoop[_data.Numer]);
                    _IsFirstLoop[_data.Numer] = false;
                    break;
            }
        }
        /// <summary>
        /// опрос всех комманд списка
        /// </summary>
        /// <param name="_Port"></param>
        /// <param name="_Commnads"></param>
        /// <param name="_TimerNumer"></param>
        private void CommandPolling(SerialPort _Port, List<Comand_COM> _Commnads, int _TimerNumer, bool _IsFirstLoop)
        {
            if (_Port.IsOpen && _Commnads != null && _IsPrevDone[_TimerNumer])
            {
                _IsPrevDone[_TimerNumer] = false;
                int _CounterCommandsDone = 0;
                foreach (Comand_COM _OneCommnad in _Commnads)
                {
                    if (!KillAll)
                    {
                        //если ошиблись с общением или не получили ответа - остановка цикла
                        if (!SendCommand(_OneCommnad, _IsFirstLoop, _TimerNumer, _Port).Result) break;
                        _CounterCommandsDone += 1;
                    }
                }
                //проверка все ли команды выполнены в цикле
                if (_CounterCommandsDone == _Commnads.Count)
                {
                    _IsPrevDone[_TimerNumer] = true;
                    _ErrorCount[_TimerNumer] = 0;
                }
            }
        }
        private void CommandPolling(TimerData _data, bool _IsFirstloop)
        {
            if (_data.CList != null && _IsPrevDone[_data.Numer])
            {
                _IsPrevDone[_data.Numer] = false;
                int _CounterCommandsDone = 0;
                foreach (Comand_COM _OneCommnad in _data.CList)
                {
                    if (!KillAll)
                    {
                        //если ошиблись с общением или не получили ответа - остановка цикла
                        if (!SendCommand(_OneCommnad, _IsFirstloop, _data.Numer, _data.IP, _data.port)) break;
                        _CounterCommandsDone += 1;
                    }
                }
                //проверка все ли команды выполнены в цикле
                if (_CounterCommandsDone == _data.CList.Count)
                {
                    _IsPrevDone[_data.Numer] = true;
                    _ErrorCount[_data.Numer] = 0;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Command"></param>
        /// <param name="_IsFirst"></param>
        /// <param name="_LoopNumber"></param>
        /// <param name="_Port"></param>
        /// <returns></returns>
        private async Task<bool> SendCommand(Comand_COM _Command, bool _IsFirst, int _LoopNumber, SerialPort _Port)
        {

            //посылаем запрос и читаем ответ
            COMResponse _Response = OneCommandProcess(_Command, _Port, false, "", 0).Result; //ЗАГЛУШКА нужно получить комплекс парсе из типа команды

            //проверка на команду извне
            string _writeCommand = GetCommand(_Command.PartType,_Command.SubAdress,_Command.IdComand);
            if (_writeCommand != "")
            {
                var _res = await SendReadCOM(_writeCommand, _Port, false, false);
                Log(false, false, $"loggeg ans from COMWrite ={_res}");
            }
            return true; //потом убрать или сделать 2 режима дятел - дольить даже при ошибках или один раз - стоп
        }
        private string GetCommand(PartType partType, int unitId, int idCommand)
        {
            string ans = "";
            if (WriteCommands.ContainsKey(partType) && WriteCommands[partType].ContainsKey(unitId) &&
                WriteCommands[partType][unitId].ContainsKey(idCommand))
            {
                //WriteCommands[partType][unitId].TryGetValue(idCommand, out ans);
                WriteCommands[partType][unitId].TryRemove(idCommand, out ans);
            }
            return ans;
        }
        private bool SendCommand(Comand_COM _Command, bool _IsFirst, int _LoopNumber, string _IPAdress, int _TCPPort)
        {
            if ((_Command.isInitial && _IsFirst) || (!_Command.isInitial))
            {
                //посылаем запрос и читаем ответ
                COMResponse _Response = OneCommandProcess(_Command, null, false, _IPAdress, _TCPPort).Result; //ЗАГЛУШКА нужно получить комплекс парсе из типа команды
                if (_Response.NeedBreak || _Response.NeedErrorProcces)
                {
                    Log(true, true, $"critical error TCP {_IPAdress}:{_TCPPort}, {_Response.message}");
                    ErrorDeal(_Response.NeedBreak, _LoopNumber);
                    return false;
                }
                return true;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// посылка команды СОМ и получение ответа с использованием await readTASK
        /// </summary>
        /// <param name="comand"></param>
        /// <param name="port"></param>
        /// <param name="is_comlex"></param>
        /// <returns></returns>
        private async Task<string> SendReadCOM(string comand, SerialPort port, bool is_comlex, bool IsAwaitAnswer)
        {
            if (port.IsOpen && comand != "")
            {
                port.DiscardInBuffer();
                string ans_message_com;
                if (is_comlex)
                {
                    byte[] mess = ComplexHEXParse(comand);
                    port.Write(mess, 0, mess.Length);
                }
                else
                {
                    port.WriteLine(comand);
                }
                if (IsAwaitAnswer)
                    ans_message_com = await ReadTask(port);
                else
                    ans_message_com = "";
                return ans_message_com;
            }
            else return "";
        }
        /// <summary>
        /// чтение из порта
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        private Task<string> ReadTask(SerialPort port)
        {
            string message;
            try
            {
                if (port.IsOpen)
                {
                    message = port.ReadLine();
                }
                else message = "port closed";
            }
            catch (TimeoutException)
            {
                message = "timeout";
            }
            return Task.FromResult(message);
        }
        private byte[] ComplexHEXParse(string comand)
        {
            //строка вида 00-00-AA-CC-FF
            //нужно распарсить в unicode
            string[] hex_comand = comand.Split('-');
            int NumberChars = hex_comand.Length;
            byte[] bytes = new byte[NumberChars];
            for (int i = 0; i < NumberChars; i += 1)
            {
                bytes[i] = Convert.ToByte(hex_comand[i], 16);
            }
            return bytes;
        }
        private async Task<COMResponse> OneCommandProcess(Comand_COM _Comand, SerialPort _Port, bool _IsComplexPars, string _IPAdress, int _TCPport)
        {
            COMResponse _COMAnswer = new()
            {
                ans = "",
                NeedBreak = false,
                NeedErrorProcces = false,
                message = ""
            };
            //переключатель TCP/COM
            var _ProcessStatus = new Task<string>(() => { return ""; });
            if (_IPAdress == "" && _Port != null)
            {
                string _comanda = GetModuleCommand(_Comand, true, 0);
                Log(true, false, $"send {_Port.PortName}: {_comanda}");
                _ProcessStatus = SendReadCOM(_comanda, _Port, _IsComplexPars, true);
            }
            else
            {
                //TODO switch для TCP устройств                
            }
            if (_ProcessStatus.Status != TaskStatus.Faulted)
            {
                string _AnsString = _ProcessStatus.Result;
                if (_Port != null)
                {
                    Log(true, false, $"recv {_Port.PortName}: {_AnsString}");
                }
                else Log(true, false, $"recv {_IPAdress}:{_TCPport}: {_AnsString}");
                if (_AnsString != "timeout" && _AnsString != "port closed" && _AnsString != "")
                {
                    //отсылаем ответ в Ergo
                    using (var scope = _scopeFactory.CreateScope())
                    {
                        var serv = scope.ServiceProvider.GetRequiredService<ICommunicateService>();
                        await serv.SendMessage(_AnsString, Id, _Comand.PartType, _Comand.SubAdress);
                    }
                    return _COMAnswer;
                }
                else
                {
                    _COMAnswer.message = "timeout or no answer " + _Port.PortName;
                    _COMAnswer.NeedErrorProcces = true;
                }
            }
            else
            {
                _COMAnswer.message = "readtask for read was Faulted " + _Port.PortName;
                _COMAnswer.NeedErrorProcces = true;
            }
            return _COMAnswer;
        }
        /// <summary>
        /// Обработка ошибки
        /// </summary>
        /// <param name="IsCritical"></param>
        /// <param name="portnum"></param>
        private void ErrorDeal(bool IsCritical, int portnum)
        {
            if (IsCritical)
            {
                //команда в Стайт машину на смену состояния на error TODO
            }
            else
            {
                if (_ErrorCount[portnum] >= MaxErrorCount)
                {
                    Log(true, true, $"second error in port {portnum}");
                    //команда в Стайт машину на смену состояния на error TODO
                }
                else
                {
                    Log(true, true, $"first error - wait next, port = {portnum}");
                    _ErrorCount[portnum] += 1;
                }
            }
        }
        private string GetModuleCommand(Comand_COM _Com, bool _IsRead, int _value)
        {
            string ans = "";
            switch (_Com.IdComand)
            {
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                    if (_IsRead)
                        ans = "";// _Pfparser.GetReadCommand(_Com.SubAdress, _Com.IdComand);
                    else
                        ans = "";// _Pfparser.GetWriteCommand(_Com.SubAdress, _Com.IdComand, _value);
                    break;
                case 109:
                case 110:
                case 111:
                case 112:
                case 113:
                case 114:
                case 115:
                case 116:
                case 117:
                case 118:
                case 119:
                case 120:
                case 121:
                case 122:
                case 123:
                case 124:
                    //I-7045D
                    if (_IsRead)
                        ans = $"${_Com.SubAdress:d2}6";
                    else
                    {
                        if (_Com.IdComand - 108 <= 8)
                        {
                            ans = $"#{_Com.SubAdress:d2}A{_Com.IdComand - 109}{_value:d2}";
                        }
                        else
                        {
                            ans = $"#{_Com.SubAdress:d2}B{_Com.IdComand - 109 - 8}{_value:d2}";
                        }
                    }
                    break;
            }
            return ans;
        }
        public void AddCommand(PartType partType, int unitId, int idCommand, string _cc)
        {
            if (!WriteCommands.ContainsKey(partType))
            {
                WriteCommands.TryAdd(partType, new ConcurrentDictionary<int, ConcurrentDictionary<int, string>> { });
            }
            if (!WriteCommands[partType].ContainsKey(unitId)) 
            {
                WriteCommands[partType].TryAdd(unitId, new ConcurrentDictionary<int, string> {  });
            }
            switch (idCommand)
            {
                //это для ICPDAS где одна комманда- несколько параметров
                case 109:
                case 110:
                case 111:
                case 112:
                case 113:
                case 114:
                case 115:
                case 116:
                case 117:
                case 118:
                case 119:
                case 120:
                case 121:
                case 122:
                case 123:
                case 124:
                    WriteCommands[partType][unitId].AddOrUpdate(109, _cc, (key, val) => _cc);
                    break;
                default:
                    WriteCommands[partType][unitId].AddOrUpdate(idCommand, _cc, (key, val) => _cc);
                    break;
            }
            
        }
    }
}
