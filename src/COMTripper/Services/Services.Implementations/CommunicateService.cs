﻿using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Options;
using System.Net.Http.Json;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;

namespace Services.Implementations
{
    public class CommunicateService :ICommunicateService
    {
        private readonly Microsoft.Extensions.Logging.ILogger<CommunicateService> _logger;
        IHttpClientFactory _httpClientFactory;
        private CommandServiceSettings _settings;
        private readonly IConfiguration _configuration;
        public CommunicateService(Microsoft.Extensions.Logging.ILogger<CommunicateService> logger, IHttpClientFactory httpClientFactory, 
            IOptions<CommandServiceSettings> options, IConfiguration configuration)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _settings = options.Value;
            _configuration = configuration;
        }
        public async Task<string> SendMessage(string message, Guid setupId, PartType partType, int unitId, CancellationToken token = default)
        {
            using HttpClient _client = _httpClientFactory.CreateClient();
            try
            {
                var data = new ResponseData
                {
                    Idclient = setupId,
                    IdPartType = partType,
                    IdUnit = unitId,
                    IdParameter = 0,
                    Command = message
                };
                using var sendResult = await _client.PostAsJsonAsync<ResponseData>(_settings.ProxyAdress, data, token);

                if (!sendResult.IsSuccessStatusCode)
                {
                    var errorResult = await sendResult.Content?.ReadFromJsonAsync<ResponseData>();
                     _logger.LogInformation($"Не удалось выполнить запрос {(int)sendResult.StatusCode}");
                    return "smthg wrong in post async";
                }

                return "all good in postasync";
            }
            catch (Exception e)
            {
                _logger.LogInformation("Error getting State Info: {Error}", e);
            }
            return "smthg wrong in post async";
        }
        public async Task StartStateMAchine()
        {
            using HttpClient _client = _httpClientFactory.CreateClient();
            try
            {
                var sendResult = await _client.GetStringAsync($"http://host.docker.internal:8000/StateData/start/{_configuration["SetupID"]}");
            }
            catch (Exception e)
            {
                _logger.LogInformation("Error starting StateMachine: {Error}", e);
            }
        }
    }
}
