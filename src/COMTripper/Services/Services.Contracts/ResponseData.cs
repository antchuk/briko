﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class ResponseData
    {
        public Guid Idclient { get; set; }
        /// <summary>
        /// тип устройства
        /// </summary>		
        public PartType IdPartType { get; set; }
        /// <summary>
        /// ид или номер устройства (если их несколько)
        /// </summary>
        public int IdUnit { get; set; }
        /// <summary>
        /// параметр, если 0 - значит команда общая
        /// </summary>
        public int IdParameter { get; set; }
        /// <summary>
        /// данные для парсинга
        /// </summary>
        public string Command { get; set; }
    }
}
