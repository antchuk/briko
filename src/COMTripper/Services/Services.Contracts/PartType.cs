﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public enum PartType
    {
        Gauge,
        UPC,
        Switcher,
        DO7045,
        Chamber,
        TurbomolecularPump,
        ScrollPump
    }
}
