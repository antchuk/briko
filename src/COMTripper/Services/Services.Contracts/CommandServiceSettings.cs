﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class CommandServiceSettings
    {
        public string ProxyAdress { get; set; }
        public string OgreAdress { get; set; }
    }
}
