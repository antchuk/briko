﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    [Serializable]
    public struct ComPort
    {
        public string Name { get; set; }
        public int Baudrate { get; set; }
        public int Parity { get; set; }
        public int Databits { get; set; }
        public int Stopbits { get; set; }
        public bool Is_handshake { get; set; }
        public int Readtimeout { get; set; }
        public int Writetimeout { get; set; }
        public string EndLine { get; set; }
        public bool DtrEnable { get; set; }
        public bool RtsEnable { get; set; }
    }
}
