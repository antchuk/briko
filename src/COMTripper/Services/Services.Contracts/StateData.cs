﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace Services.Contracts
{
    /// <summary>
    /// информация о состоянии
    /// </summary>
    [Serializable]
    public struct StateData
    {
        public List<List<Comand_COM>> ComandsList;
        public List<COMTimer_settings> COMTmrSettings;
        public List<ComPort> COMSettings;
    }
    /// <summary>
    /// команда COM
    /// </summary>
    [Serializable]
    public struct Comand_COM
    {
        public int IdComand;
        public string Name;
        public bool isInitial; //выполняется только 1 раз при запуске цикла
        public int SubAdress; //idunit
        public bool IsWrite;
        public PartType PartType; //тип узла

    }
    /// <summary>
    /// настройка 1 таймера
    /// </summary>
    [Serializable]
    public struct COMTimer_settings
    {
        public int Type;
        public int period;
        public int start_delay;
    }
    /// <summary>
    /// информация для тика таймера 1 - ASCII, 2 - ModbusTCP
    /// </summary>
    [Serializable]
    public struct TimerData
    {
        public int TypeOfTimer;
        public SerialPort COMPort;
        public List<Comand_COM> CList;
        public int Numer;
        public System.Threading.TimerCallback callback;
        public string IP;
        public int port;
    }
    /// <summary>
    /// ответ был ли успешен опрос COM порта
    /// </summary>
    [Serializable]
    public struct COMResponse
    {
        public string ans;
        public bool NeedBreak;
        public bool NeedErrorProcces;
        public string message;
    }
}
