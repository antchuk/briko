﻿using System.Net;
using System.Net.Http;


namespace Providers
{
	public class ProviderBase
	{
		internal async Task<TResponse> PostAsync<TRequest, TResponse>(
		IHttpClientFactory clientFactory,
		string clientName,
		TRequest request,
		string relativePath,
		CancellationToken cancellationToken) where TResponse : ResponseBase, new()
		{
			cancellationToken.ThrowIfCancellationRequested();
			var client = clientFactory.CreateClient(clientName);

			try
			{
				using var message = await client.PostAsJsonAsync<TRequest>(relativePath, request, cancellationToken);

				if (!message.IsSuccessStatusCode)
				{
					var errorResult = await message.Content?.ReadFromJsonAsync<ResponseBase>();

					if (string.IsNullOrEmpty(errorResult?.ErrorMessage))
					{
						throw new BadHttpRequestException("Не удалось выполнить запрос.", (int)message.StatusCode);
					}

					return new TResponse() { ErrorMessage = errorResult.ErrorMessage };
				}

				var result = await message.Content?.ReadFromJsonAsync<TResponse>();

				return result;
			}
			catch (Exception ex)
			{
				return new TResponse() { ErrorMessage = "Не удалось выполнить запрос." };
			}
		}

		internal async Task<TResponse> GetAsync<TResponse>(
			IHttpClientFactory clientFactory,
			string clientName,
			string relativePath,
			CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			var client = clientFactory.CreateClient(clientName);
			try
			{
				using var message = await client.GetAsync(relativePath, cancellationToken);

				if (!message.IsSuccessStatusCode)
				{
					var errorResult = await message.Content.ReadAsStringAsync();
					throw new BadHttpRequestException(errorResult, (int)message.StatusCode);
				}

				var result = await message.Content?.ReadFromJsonAsync<TResponse>();

				return result;
			}
			catch (Exception ex)
			{
				throw new BadHttpRequestException($"Не удалось выполнить запрос. {ex.Message}");
			}
		}

		internal async Task<TResponse> PutAsync<TRequest, TResponse>(
			IHttpClientFactory clientFactory,
			string clientName,
			TRequest request,
			string relativePath,
			CancellationToken cancellationToken) where TResponse : ResponseBase, new()
		{
			cancellationToken.ThrowIfCancellationRequested();
			var client = clientFactory.CreateClient(clientName);

			try
			{
				using var message = await client.PutAsJsonAsync<TRequest>(relativePath, request, cancellationToken);

				if (!message.IsSuccessStatusCode)
				{
					var errorResult = await message.Content?.ReadFromJsonAsync<ResponseBase>();

					if (string.IsNullOrEmpty(errorResult?.ErrorMessage))
					{
						throw new BadHttpRequestException("Не удалось выполнить запрос.", (int)message.StatusCode);
					}

					return new TResponse() { ErrorMessage = errorResult.ErrorMessage };
				}

				var result = await message.Content?.ReadFromJsonAsync<TResponse>();

				return result;
			}
			catch (Exception ex)
			{
				return new TResponse() { ErrorMessage = "Не удалось выполнить запрос." };
			}
		}


		internal async Task<TResponse> GetResultOrDefaultAsync<TResponse>(
		IHttpClientFactory clientFactory,
		string clientName,
		string relativePath,
		CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			var client = clientFactory.CreateClient(clientName);
			try
			{
				using var message = await client.GetAsync(relativePath, cancellationToken);

				if (!message.IsSuccessStatusCode)
				{
					var errorResult = await message.Content.ReadAsStringAsync();
					throw new BadHttpRequestException(errorResult, (int)message.StatusCode);
				}

				if (message.StatusCode == HttpStatusCode.NoContent)
					return default(TResponse);

				var result = await message.Content?.ReadFromJsonAsync<TResponse>();

				return result;
			}
			catch (Exception ex)
			{
				throw new BadHttpRequestException($"Не удалось выполнить запрос. {ex.Message}");
			}
		}
	}
}
