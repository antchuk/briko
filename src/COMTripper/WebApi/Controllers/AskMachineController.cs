﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер состояния о установке
    /// </summary>
    [ApiController]
    //[Route("StateData")]
    public class AskMachineController : ControllerBase
    {
        private readonly IAskMachineService _askMachineService;
        private readonly Microsoft.Extensions.Logging.ILogger<AskMachineController> _logger;
        private readonly ICommunicateService _communicateService;
        public AskMachineController(IAskMachineService service, Microsoft.Extensions.Logging.ILogger<AskMachineController> logger, ICommunicateService communicateService)
        {
            _askMachineService = service;
            _logger = logger;
            _communicateService = communicateService;
            //_mapper = mapper;
        }
        /// <summary>
        /// Запуск машины опроса железа
        /// </summary>
        /// <returns></returns>
        [HttpGet("startmachine")]
        public async Task<IActionResult> GetStart()
        {
            await _askMachineService.StartMachine();
            return Ok();
        }
        /// <summary>
        /// Стоп машины опроса железа
        /// </summary>
        /// <returns></returns>
        [HttpGet("stopmachine")]
        public async Task<IActionResult> GetStop()
        {
            await _askMachineService.StopMachine();
            return Ok();
        }
        [HttpPost("recievecommand")]
        public async Task<IActionResult> PostComand([FromBody] ResponseData data)
        {
            if (data == null || data.Idclient == Guid.Empty)
            {
                _logger.LogInformation($"get null or empty GUID");
                return BadRequest();
            }
            _logger.LogInformation($"get meassage = {data.Command}");
            _askMachineService.AddCommand(data.IdPartType, data.IdUnit, data.IdParameter, data.Command);
            return Ok();
        }
    }
}
