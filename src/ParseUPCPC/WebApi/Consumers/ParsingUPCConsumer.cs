﻿using AutoMapper;
using MassTransit;
using Services.Abstractions;
using Services.Contracts;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System;
using WebApi.Models;
using MassTransit.Configuration;
using System.Collections.Generic;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace WebApi.Consumers
{
	/// <summary>
	/// Получатель сообщений
	/// </summary>
	public class ParsingUPCConsumer : IConsumer<DataIn>
	{
		private readonly IParserService	_service;
		private readonly IMapper _mapper;
		private readonly Microsoft.Extensions.Logging.ILogger<ParsingUPCConsumer> _logger;
		private readonly ISendEndpointProvider _sendEndpointProvider;
		private readonly QueueNames _queueNames;

		public ParsingUPCConsumer(
			IParserService service,
			IMapper mapper,
			Microsoft.Extensions.Logging.ILogger<ParsingUPCConsumer> logger,
			ISendEndpointProvider sendEndpointProvider,
			IOptions<QueueNames> queuenames)
		{
			_service = service;
			_mapper = mapper;
			_logger = logger;
			_sendEndpointProvider = sendEndpointProvider;
			_queueNames = queuenames.Value;
		}
		public async Task Consume(ConsumeContext<DataIn> context)
		{
			_logger.LogInformation($"we have got message {context.Message.Data}");
			if (context == null)
			{
                _logger.LogInformation($"null context");
                //послать сообщение об ошибке в очередь ошибок?
                return;
			}
			if (context.Message.IDSetup == Guid.Empty)
			{
                _logger.LogInformation($"empty guid");
                //послать сообщение об ошибке в очередь ошибок?
                return;
			}
			if (context.Message.IDPartType == 0)
			{
                _logger.LogInformation($"null partType");
                //послать сообщение об ошибке в очередь ошибок?
                return;
			}
			if (context.Message.Data == null)
			{
				//послать сообщение об ошибке в очередь ошибок?
				return;
			}
			if (context.Message.Data.Length < 30)
			{
                _logger.LogInformation($"error in data length 37");
                //послать сообщение об ошибке в очередь ошибок?
                return;
			}
			//получить
			//здеcь идем в сервис за парсингом
			var upcdata = await _service.Parse(context.Message.Data);
			if (upcdata.succed)
			{
				_logger.LogInformation($"we parse I_p freequency {upcdata.Data[5]}");
				//отправляем в очередь qStateData

				var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri($"exchange:{_queueNames.QStateData}"));
				await endpoint.Send<UPCdto>(new UPCdto(upcdata.succed, context.Message.IDSetup, context.Message.IDPartType, context.Message.IDUnit, upcdata.Data));
			}
			else
			{
				_logger.LogInformation($"parsing problem in client = {context.Message.IDSetup} par = {context.Message.IDPartType}" );
			}
		}
		/// <summary>
		/// Отправка сообщения в EndPoint(queue or Exchange)
		/// </summary>
		/// <returns></returns>
		private async Task Send(UPCdto data, Guid id, float param) //CancellationToken stoppingtoken
		{
			//var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri($"exchange:{_queueNames.QStateData}"));
			//await endpoint.Send<UPCdto>(new UPCdto(data.succed,id,param,data.Data));
		}
	}
	/// <summary>
	/// не создает дополнителные Exchange/queue в RabbitMQ
	/// </summary>
	public class ParsingUPCDefinition : ConsumerDefinition<ParsingUPCConsumer>
	{
		protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<ParsingUPCConsumer> consumerConfigurator)
		{
			endpointConfigurator.ConfigureConsumeTopology = false; //не создается Exchange для NotificationDto
		}
	}
}
