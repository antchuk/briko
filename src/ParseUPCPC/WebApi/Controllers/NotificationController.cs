﻿using AutoMapper;
using Domain.Entities;
using MassTransit;
using MassTransit.Transports;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Settings;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер уведомлений
    /// </summary>
    [ApiController]
    [Route("NotificationService/notification")]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService _service;
        private readonly IMapper _mapper;
        private readonly ISendEndpointProvider _sendEndpointProvider;
        public NotificationController(INotificationService service, IMapper mapper, ISendEndpointProvider sendEndpointProvider)
        {
            _service = service;
            _mapper = mapper;
            _sendEndpointProvider = sendEndpointProvider;
        }
        /// <summary>
        /// Получить уведомление
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            if (id ==  Guid.Empty) 
            {
                return BadRequest();
            }
            var enity = await _service.GetNotificationById(id);
            if (enity == null) 
            {
                return NotFound();
            }
            return Ok(_mapper.Map<NotificationModel>(enity));
        }
        /// <summary>
        /// Создать уведомление
        /// </summary>
        /// <param name="notificationDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] NotificationModel notificationDto)
        {
            if (notificationDto == null) 
            { 
                return  BadRequest(); 
            }
            await SendTest(notificationDto);
            return Ok();
        }
        /// <summary>
        /// Отправка сообщения в EndPoint(queue or Exchange)
        /// </summary>
        /// <returns></returns>
        private async Task SendTest(NotificationModel notificationDto) //CancellationToken stoppingtoken
        {
            
            var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri("exchange:NotificationsCommon"));
            await endpoint.Send(new NotificationDto 
            {  
                Body = notificationDto.Body,
                UserID = notificationDto.UserID,
                SendingDateTime = notificationDto.SendingDateTime
            });
        }
    }
}
