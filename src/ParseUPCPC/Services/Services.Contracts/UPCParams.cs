﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class UPCParams
	{
		public Dictionary<int, float> Data { get; set; }
		public bool succed { get; set; }
	}
}
