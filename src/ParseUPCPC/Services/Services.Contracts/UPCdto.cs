﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public class UPCdto
	{
		public Dictionary<int, float> Data { get; init; }
		public bool succed { get; init; }
		public Guid IDSetup { get; init; }
		public int IDPartType { get; init; }
		public int IDUnit {  get; init; }

		public UPCdto(bool succ, Guid IDCl, int idP, int idU , Dictionary<int, float> D)
		{
			succed = succ;
			IDSetup = IDCl;
			IDPartType = idP;
			IDUnit = idU;
			Data = D;
		}
	}
}
