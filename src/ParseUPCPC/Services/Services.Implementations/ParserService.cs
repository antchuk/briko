﻿using Microsoft.IdentityModel.Tokens;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using System.Globalization;

namespace Services.Implementations
{	
	/// <summary>
	/// Сервис парсинга ответа модуля UPC
	/// </summary>
	public class ParserService : IParserService
	{
		private readonly Microsoft.Extensions.Logging.ILogger<ParserService> _logger;
		public ParserService(Microsoft.Extensions.Logging.ILogger<ParserService> logger) 
		{
			_logger = logger;
		}
		public async Task<UPCParams> Parse(string input) 
		{
			var ans = await ParseAsync(input);
			return ans;
		}
		private Task<UPCParams> ParseAsync(string input) 
		{
			/*if (input == null) 
			{
				//сообщение?
				return Task.FromResult(-999f);
			}
			if (input.Length < 37)
			{
				//сообщение?
				return Task.FromResult(-999f);
			}*/
			UPCParams Upc = Parsedata(input);
			if (!Upc.succed)
			{
				return Task.FromResult(Upc);
			}
			return Task.FromResult(Upc);
		}
		/// <summary>
		/// парсинг строки
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		private UPCParams Parsedata(string input)
		{
			try
			{
				//TODO парсинг поменять
				float p1 = float.Parse(input.Substring(1, 5), NumberStyles.Any, CultureInfo.InvariantCulture);
				//_logger.LogInformation($"parse 1 = {p1}");
				float p2 = float.Parse(input.Substring(7, 5), NumberStyles.Any, CultureInfo.InvariantCulture);
				float p3 = float.Parse(input.Substring(13, 5), NumberStyles.Any, CultureInfo.InvariantCulture);
				float p4 = float.Parse(input.Substring(19, 3), NumberStyles.Any, CultureInfo.InvariantCulture);
				float p5 = float.Parse(input.Substring(23, 4), NumberStyles.Any, CultureInfo.InvariantCulture);
				float p6 = float.Parse(input.Substring(28, 4), NumberStyles.Any, CultureInfo.InvariantCulture);
				float p7 = float.Parse(input.Substring(33, 4), NumberStyles.Any, CultureInfo.InvariantCulture);
				float p8 = float.Parse(input.Substring(38, 4), NumberStyles.Any, CultureInfo.InvariantCulture);

				UPCParams Upc = new() {
					Data = new Dictionary<int, float> { { 1, p1 }, { 2, p2 }, { 3, p3 }, { 4, p4 }, { 5, p5 }, { 6, p6 }, { 7, p7 }, { 8, p8 } },
					succed = true };
				return Upc;
			}
			catch (FormatException exx)
			{
				_logger.LogInformation($"FORMAT error = {exx.ToString}");
				//лог о ошибке
				UPCParams uPCdata = new()
				{
					succed = false
				};
				return null;
			}
			catch (Exception ex)
			{
                _logger.LogInformation($"error = {ex.ToString}");
                //лог о ошибке
                UPCParams uPCdata = new()
				{
					succed = false
				};
				return null;
			}			
		}
	}
}
